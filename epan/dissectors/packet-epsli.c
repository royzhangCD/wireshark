/* Do not modify this file. Changes will be overwritten.                      */
/* Generated automatically by the ASN.1 to Wireshark dissector compiler       */
/* packet-epsli.c                                                             */
/* asn2wrs.py -b -p epsli -c ./epsli.cnf -s ./packet-epsli-template -D . -O ../.. HI2Operations.asn UmtsHI2Operations.asn EpsHI2Operations.asn EpsHI3Operations.asn */

/* Input file: packet-epsli-template.c */

#line 1 "./asn1/epsli/packet-epsli-template.c"
/* packet-epsli-template.c
 * Routines for Lawful Interception X2 xIRI event dissection
 *
 * See 3GPP TS33.128.
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/conversation.h>
#include <epan/asn1.h>

#include "packet-ber.h"

#define PNAME  "EPS Lawful Interception"
#define PSNAME "EPSLI"
#define PFNAME "epsli"   

void proto_reg_handoff_epsli(void);
void proto_register_epsli(void);

/* Initialize the protocol and registered fields */
static int proto_epsli = -1;
static gint ett_iri = -1;
static dissector_handle_t ip_handle;
static dissector_handle_t ipv6_handle;
//static dissector_handle_t epsli_handle = NULL;



/*--- Included file: packet-epsli-hf.c ---*/
#line 1 "./asn1/epsli/packet-epsli-hf.c"
static int hf_epsli_EpsIRIContent_PDU = -1;       /* EpsIRIContent */
static int hf_epsli_CC_PDU_PDU = -1;              /* CC_PDU */
static int hf_epsli_localTime = -1;               /* LocalTimeStamp */
static int hf_epsli_utcTime = -1;                 /* UTCTime */
static int hf_epsli_generalizedTime = -1;         /* GeneralizedTime */
static int hf_epsli_winterSummerIndication = -1;  /* T_winterSummerIndication */
static int hf_epsli_operator_Identifier = -1;     /* OCTET_STRING_SIZE_1_5 */
static int hf_epsli_network_Element_Identifier = -1;  /* Network_Element_Identifier */
static int hf_epsli_e164_Format = -1;             /* OCTET_STRING_SIZE_1_25 */
static int hf_epsli_x25_Format = -1;              /* OCTET_STRING_SIZE_1_25 */
static int hf_epsli_iP_Format = -1;               /* OCTET_STRING_SIZE_1_25 */
static int hf_epsli_dNS_Format = -1;              /* OCTET_STRING_SIZE_1_25 */
static int hf_epsli_iP_Address = -1;              /* IPAddress */
static int hf_epsli_National_Parameters_item = -1;  /* OCTET_STRING_SIZE_1_256 */
static int hf_epsli_countryCode = -1;             /* PrintableString_SIZE_2 */
static int hf_epsli_ipAddress = -1;               /* IPAddress */
static int hf_epsli_x25Address = -1;              /* X25Address */
static int hf_epsli_iP_type = -1;                 /* T_iP_type */
static int hf_epsli_iP_value = -1;                /* IP_value */
static int hf_epsli_iP_assignment = -1;           /* T_iP_assignment */
static int hf_epsli_iPBinaryAddress = -1;         /* OCTET_STRING_SIZE_4_16 */
static int hf_epsli_iPTextAddress = -1;           /* IA5String_SIZE_7_45 */
static int hf_epsli_detailedCivicAddress = -1;    /* SET_OF_DetailedCivicAddress */
static int hf_epsli_detailedCivicAddress_item = -1;  /* DetailedCivicAddress */
static int hf_epsli_xmlCivicAddress = -1;         /* XmlCivicAddress */
static int hf_epsli_building = -1;                /* UTF8String */
static int hf_epsli_room = -1;                    /* UTF8String */
static int hf_epsli_placeType = -1;               /* UTF8String */
static int hf_epsli_postalCommunityName = -1;     /* UTF8String */
static int hf_epsli_additionalCode = -1;          /* UTF8String */
static int hf_epsli_seat = -1;                    /* UTF8String */
static int hf_epsli_primaryRoad = -1;             /* UTF8String */
static int hf_epsli_primaryRoadDirection = -1;    /* UTF8String */
static int hf_epsli_trailingStreetSuffix = -1;    /* UTF8String */
static int hf_epsli_streetSuffix = -1;            /* UTF8String */
static int hf_epsli_houseNumber = -1;             /* UTF8String */
static int hf_epsli_houseNumberSuffix = -1;       /* UTF8String */
static int hf_epsli_landmarkAddress = -1;         /* UTF8String */
static int hf_epsli_additionalLocation = -1;      /* UTF8String */
static int hf_epsli_name = -1;                    /* UTF8String */
static int hf_epsli_floor = -1;                   /* UTF8String */
static int hf_epsli_primaryStreet = -1;           /* UTF8String */
static int hf_epsli_primaryStreetDirection = -1;  /* UTF8String */
static int hf_epsli_roadSection = -1;             /* UTF8String */
static int hf_epsli_roadBranch = -1;              /* UTF8String */
static int hf_epsli_roadSubBranch = -1;           /* UTF8String */
static int hf_epsli_roadPreModifier = -1;         /* UTF8String */
static int hf_epsli_roadPostModifier = -1;        /* UTF8String */
static int hf_epsli_postalCode = -1;              /* UTF8String */
static int hf_epsli_town = -1;                    /* UTF8String */
static int hf_epsli_county = -1;                  /* UTF8String */
static int hf_epsli_country = -1;                 /* UTF8String */
static int hf_epsli_language = -1;                /* UTF8String */
static int hf_epsli_posMethod = -1;               /* PrintableString */
static int hf_epsli_mapData = -1;                 /* T_mapData */
static int hf_epsli_base64Map = -1;               /* PrintableString */
static int hf_epsli_url = -1;                     /* PrintableString */
static int hf_epsli_altitude = -1;                /* T_altitude */
static int hf_epsli_alt = -1;                     /* PrintableString */
static int hf_epsli_alt_uncertainty = -1;         /* PrintableString */
static int hf_epsli_speed = -1;                   /* PrintableString */
static int hf_epsli_direction = -1;               /* PrintableString */
static int hf_epsli_level_conf = -1;              /* PrintableString */
static int hf_epsli_qOS_not_met = -1;             /* BOOLEAN */
static int hf_epsli_motionStateList = -1;         /* T_motionStateList */
static int hf_epsli_primaryMotionState = -1;      /* PrintableString */
static int hf_epsli_secondaryMotionState = -1;    /* T_secondaryMotionState */
static int hf_epsli_secondaryMotionState_item = -1;  /* PrintableString */
static int hf_epsli_confidence = -1;              /* PrintableString */
static int hf_epsli_floor_01 = -1;                /* T_floor */
static int hf_epsli_floor_number = -1;            /* PrintableString */
static int hf_epsli_floor_number_uncertainty = -1;  /* PrintableString */
static int hf_epsli_additional_info = -1;         /* PrintableString */
static int hf_epsli_lALS_rawMLPPosData = -1;      /* UTF8String */
static int hf_epsli_keep_Alive = -1;              /* NULL */
static int hf_epsli_iRI_Begin_record = -1;        /* IRI_Parameters */
static int hf_epsli_iRI_End_record = -1;          /* IRI_Parameters */
static int hf_epsli_iRI_Continue_record = -1;     /* IRI_Parameters */
static int hf_epsli_iRI_Report_record = -1;       /* IRI_Parameters */
static int hf_epsli_hi2epsDomainId = -1;          /* OBJECT_IDENTIFIER */
static int hf_epsli_lawfulInterceptionIdentifier = -1;  /* LawfulInterceptionIdentifier */
static int hf_epsli_timeStamp = -1;               /* TimeStamp */
static int hf_epsli_initiator = -1;               /* T_initiator */
static int hf_epsli_locationOfTheTarget = -1;     /* Location */
static int hf_epsli_partyInformation = -1;        /* SET_SIZE_1_10_OF_PartyInformation */
static int hf_epsli_partyInformation_item = -1;   /* PartyInformation */
static int hf_epsli_serviceCenterAddress = -1;    /* PartyInformation */
static int hf_epsli_sMS = -1;                     /* SMS_report */
static int hf_epsli_national_Parameters = -1;     /* National_Parameters */
static int hf_epsli_ePSCorrelationNumber = -1;    /* EPSCorrelationNumber */
static int hf_epsli_ePSevent = -1;                /* EPSEvent */
static int hf_epsli_sgsnAddress = -1;             /* DataNodeAddress */
static int hf_epsli_gPRSOperationErrorCode = -1;  /* GPRSOperationErrorCode */
static int hf_epsli_ggsnAddress = -1;             /* DataNodeAddress */
static int hf_epsli_qOS = -1;                     /* UmtsQos */
static int hf_epsli_networkIdentifier = -1;       /* Network_Identifier */
static int hf_epsli_sMSOriginatingAddress = -1;   /* DataNodeAddress */
static int hf_epsli_sMSTerminatingAddress = -1;   /* DataNodeAddress */
static int hf_epsli_iMSevent = -1;                /* IMSevent */
static int hf_epsli_sIPMessage = -1;              /* OCTET_STRING */
static int hf_epsli_servingSGSN_number = -1;      /* OCTET_STRING_SIZE_1_20 */
static int hf_epsli_servingSGSN_address = -1;     /* OCTET_STRING_SIZE_5_17 */
static int hf_epsli_ldiEvent = -1;                /* LDIevent */
static int hf_epsli_correlation = -1;             /* CorrelationValues */
static int hf_epsli_ePS_GTPV2_specificParameters = -1;  /* EPS_GTPV2_SpecificParameters */
static int hf_epsli_ePS_PMIP_specificParameters = -1;  /* EPS_PMIP_SpecificParameters */
static int hf_epsli_ePS_DSMIP_SpecificParameters = -1;  /* EPS_DSMIP_SpecificParameters */
static int hf_epsli_ePS_MIP_SpecificParameters = -1;  /* EPS_MIP_SpecificParameters */
static int hf_epsli_servingNodeAddress = -1;      /* OCTET_STRING */
static int hf_epsli_visitedNetworkId = -1;        /* UTF8String */
static int hf_epsli_mediaDecryption_info = -1;    /* MediaDecryption_info */
static int hf_epsli_servingS4_SGSN_address = -1;  /* OCTET_STRING */
static int hf_epsli_sipMessageHeaderOffer = -1;   /* OCTET_STRING */
static int hf_epsli_sipMessageHeaderAnswer = -1;  /* OCTET_STRING */
static int hf_epsli_sdpOffer = -1;                /* OCTET_STRING */
static int hf_epsli_sdpAnswer = -1;               /* OCTET_STRING */
static int hf_epsli_uLITimestamp = -1;            /* OCTET_STRING_SIZE_8 */
static int hf_epsli_packetDataHeaderInformation = -1;  /* PacketDataHeaderInformation */
static int hf_epsli_mediaSecFailureIndication = -1;  /* MediaSecFailureIndication */
static int hf_epsli_csgIdentity = -1;             /* OCTET_STRING_SIZE_4 */
static int hf_epsli_heNBIdentity = -1;            /* OCTET_STRING */
static int hf_epsli_heNBiPAddress = -1;           /* IPAddress */
static int hf_epsli_heNBLocation = -1;            /* HeNBLocation */
static int hf_epsli_tunnelProtocol = -1;          /* TunnelProtocol */
static int hf_epsli_pANI_Header_Info = -1;        /* SEQUENCE_OF_PANI_Header_Info */
static int hf_epsli_pANI_Header_Info_item = -1;   /* PANI_Header_Info */
static int hf_epsli_imsVoIP = -1;                 /* IMS_VoIP_Correlation */
static int hf_epsli_xCAPmessage = -1;             /* OCTET_STRING */
static int hf_epsli_logicalFunctionInformation = -1;  /* DataNodeIdentifier */
static int hf_epsli_ccUnavailableReason = -1;     /* PrintableString */
static int hf_epsli_carrierSpecificData = -1;     /* OCTET_STRING */
static int hf_epsli_current_previous_systems = -1;  /* Current_Previous_Systems */
static int hf_epsli_change_Of_Target_Identity = -1;  /* Change_Of_Target_Identity */
static int hf_epsli_requesting_Network_Identifier = -1;  /* OCTET_STRING */
static int hf_epsli_requesting_Node_Type = -1;    /* Requesting_Node_Type */
static int hf_epsli_serving_System_Identifier = -1;  /* OCTET_STRING */
static int hf_epsli_proSeTargetType = -1;         /* ProSeTargetType */
static int hf_epsli_proSeRelayMSISDN = -1;        /* OCTET_STRING_SIZE_1_9 */
static int hf_epsli_proSeRelayIMSI = -1;          /* OCTET_STRING_SIZE_3_8 */
static int hf_epsli_proSeRelayIMEI = -1;          /* OCTET_STRING_SIZE_8 */
static int hf_epsli_extendedLocParameters = -1;   /* ExtendedLocParameters */
static int hf_epsli_locationErrorCode = -1;       /* LocationErrorCode */
static int hf_epsli_otherIdentities = -1;         /* SEQUENCE_OF_PartyInformation */
static int hf_epsli_otherIdentities_item = -1;    /* PartyInformation */
static int hf_epsli_deregistrationReason = -1;    /* DeregistrationReason */
static int hf_epsli_requesting_Node_Identifier = -1;  /* OCTET_STRING */
static int hf_epsli_roamingIndication = -1;       /* VoIPRoamingIndication */
static int hf_epsli_cSREvent = -1;                /* CSREvent */
static int hf_epsli_ptc = -1;                     /* PTC */
static int hf_epsli_ptcEncryption = -1;           /* PTCEncryptionInfo */
static int hf_epsli_additionalCellIDs = -1;       /* SEQUENCE_OF_AdditionalCellID */
static int hf_epsli_additionalCellIDs_item = -1;  /* AdditionalCellID */
static int hf_epsli_scefID = -1;                  /* UTF8String */
static int hf_epsli_ice_type = -1;                /* ICE_type */
static int hf_epsli_national_HI2_ASN1parameters = -1;  /* National_HI2_ASN1parameters */
static int hf_epsli_dataNodeAddress = -1;         /* DataNodeAddress */
static int hf_epsli_logicalFunctionType = -1;     /* LogicalFunctionType */
static int hf_epsli_dataNodeName = -1;            /* PrintableString_SIZE_7_25 */
static int hf_epsli_access_Type = -1;             /* OCTET_STRING */
static int hf_epsli_access_Class = -1;            /* OCTET_STRING */
static int hf_epsli_network_Provided = -1;        /* NULL */
static int hf_epsli_pANI_Location = -1;           /* PANI_Location */
static int hf_epsli_raw_Location = -1;            /* OCTET_STRING */
static int hf_epsli_location = -1;                /* Location */
static int hf_epsli_ePSLocation = -1;             /* EPSLocation */
static int hf_epsli_party_Qualifier = -1;         /* T_party_Qualifier */
static int hf_epsli_partyIdentity = -1;           /* T_partyIdentity */
static int hf_epsli_imei = -1;                    /* OCTET_STRING_SIZE_8 */
static int hf_epsli_imsi = -1;                    /* OCTET_STRING_SIZE_3_8 */
static int hf_epsli_msISDN = -1;                  /* OCTET_STRING_SIZE_1_9 */
static int hf_epsli_sip_uri = -1;                 /* OCTET_STRING */
static int hf_epsli_tel_uri = -1;                 /* OCTET_STRING */
static int hf_epsli_nai = -1;                     /* OCTET_STRING */
static int hf_epsli_x_3GPP_Asserted_Identity = -1;  /* OCTET_STRING */
static int hf_epsli_xUI = -1;                     /* OCTET_STRING */
static int hf_epsli_iMPI = -1;                    /* OCTET_STRING */
static int hf_epsli_extID = -1;                   /* UTF8String */
static int hf_epsli_services_Data_Information = -1;  /* Services_Data_Information */
static int hf_epsli_e164_Number = -1;             /* OCTET_STRING_SIZE_1_25 */
static int hf_epsli_globalCellID = -1;            /* GlobalCellID */
static int hf_epsli_rAI = -1;                     /* Rai */
static int hf_epsli_gsmLocation = -1;             /* GSMLocation */
static int hf_epsli_umtsLocation = -1;            /* UMTSLocation */
static int hf_epsli_sAI = -1;                     /* Sai */
static int hf_epsli_oldRAI = -1;                  /* Rai */
static int hf_epsli_civicAddress = -1;            /* CivicAddress */
static int hf_epsli_operatorSpecificInfo = -1;    /* OCTET_STRING */
static int hf_epsli_uELocationTimestamp = -1;     /* T_uELocationTimestamp */
static int hf_epsli_timestamp = -1;               /* TimeStamp */
static int hf_epsli_timestampUnknown = -1;        /* NULL */
static int hf_epsli_nCGI = -1;                    /* NCGI */
static int hf_epsli_timeOfLocation = -1;          /* GeneralizedTime */
static int hf_epsli_mCC = -1;                     /* MCC */
static int hf_epsli_mNC = -1;                     /* MNC */
static int hf_epsli_pLMNID = -1;                  /* PLMNID */
static int hf_epsli_nRCellID = -1;                /* NRCellID */
static int hf_epsli_geoCoordinates = -1;          /* T_geoCoordinates */
static int hf_epsli_latitude = -1;                /* PrintableString_SIZE_7_10 */
static int hf_epsli_longitude = -1;               /* PrintableString_SIZE_8_11 */
static int hf_epsli_mapDatum = -1;                /* MapDatum */
static int hf_epsli_azimuth = -1;                 /* INTEGER_0_359 */
static int hf_epsli_utmCoordinates = -1;          /* T_utmCoordinates */
static int hf_epsli_utm_East = -1;                /* PrintableString_SIZE_10 */
static int hf_epsli_utm_North = -1;               /* PrintableString_SIZE_7 */
static int hf_epsli_utmRefCoordinates = -1;       /* T_utmRefCoordinates */
static int hf_epsli_utmref_string = -1;           /* PrintableString_SIZE_13 */
static int hf_epsli_wGS84Coordinates = -1;        /* OCTET_STRING */
static int hf_epsli_point = -1;                   /* GA_Point */
static int hf_epsli_pointWithUnCertainty = -1;    /* GA_PointWithUnCertainty */
static int hf_epsli_polygon = -1;                 /* GA_Polygon */
static int hf_epsli_latitudeSign = -1;            /* T_latitudeSign */
static int hf_epsli_latitude_01 = -1;             /* INTEGER_0_8388607 */
static int hf_epsli_longitude_01 = -1;            /* INTEGER_M8388608_8388607 */
static int hf_epsli_geographicalCoordinates = -1;  /* GeographicalCoordinates */
static int hf_epsli_uncertaintyCode = -1;         /* INTEGER_0_127 */
static int hf_epsli_GA_Polygon_item = -1;         /* GA_Polygon_item */
static int hf_epsli_sMS_Contents = -1;            /* T_sMS_Contents */
static int hf_epsli_sms_initiator = -1;           /* T_sms_initiator */
static int hf_epsli_transfer_status = -1;         /* T_transfer_status */
static int hf_epsli_other_message = -1;           /* T_other_message */
static int hf_epsli_content = -1;                 /* OCTET_STRING_SIZE_1_270 */
static int hf_epsli_iri_to_CC = -1;               /* IRI_to_CC_Correlation */
static int hf_epsli_iri_to_iri = -1;              /* IRI_to_IRI_Correlation */
static int hf_epsli_both_IRI_CC = -1;             /* T_both_IRI_CC */
static int hf_epsli_iri_CC = -1;                  /* IRI_to_CC_Correlation */
static int hf_epsli_iri_IRI = -1;                 /* IRI_to_IRI_Correlation */
static int hf_epsli_IMS_VoIP_Correlation_item = -1;  /* IMS_VoIP_Correlation_item */
static int hf_epsli_ims_iri = -1;                 /* IRI_to_IRI_Correlation */
static int hf_epsli_ims_cc = -1;                  /* IRI_to_CC_Correlation */
static int hf_epsli_cc = -1;                      /* T_cc */
static int hf_epsli_cc_item = -1;                 /* OCTET_STRING */
static int hf_epsli_iri = -1;                     /* OCTET_STRING */
static int hf_epsli_gPRS_parameters = -1;         /* GPRS_parameters */
static int hf_epsli_pDP_address_allocated_to_the_target = -1;  /* DataNodeAddress */
static int hf_epsli_aPN = -1;                     /* OCTET_STRING_SIZE_1_100 */
static int hf_epsli_pDP_type = -1;                /* OCTET_STRING_SIZE_2 */
static int hf_epsli_nSAPI = -1;                   /* OCTET_STRING_SIZE_1 */
static int hf_epsli_additionalIPaddress = -1;     /* DataNodeAddress */
static int hf_epsli_qosMobileRadio = -1;          /* OCTET_STRING */
static int hf_epsli_qosGn = -1;                   /* OCTET_STRING */
static int hf_epsli_pDNAddressAllocation = -1;    /* OCTET_STRING */
static int hf_epsli_protConfigOptions = -1;       /* ProtConfigOptions */
static int hf_epsli_attachType = -1;              /* OCTET_STRING_SIZE_1 */
static int hf_epsli_ePSBearerIdentity = -1;       /* OCTET_STRING */
static int hf_epsli_detachType = -1;              /* OCTET_STRING_SIZE_1 */
static int hf_epsli_rATType = -1;                 /* OCTET_STRING_SIZE_1 */
static int hf_epsli_failedBearerActivationReason = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_ePSBearerQoS = -1;            /* OCTET_STRING */
static int hf_epsli_bearerActivationType = -1;    /* TypeOfBearer */
static int hf_epsli_aPN_AMBR = -1;                /* OCTET_STRING */
static int hf_epsli_procedureTransactionId = -1;  /* OCTET_STRING */
static int hf_epsli_linkedEPSBearerId = -1;       /* OCTET_STRING */
static int hf_epsli_tFT = -1;                     /* OCTET_STRING */
static int hf_epsli_handoverIndication = -1;      /* NULL */
static int hf_epsli_failedBearerModReason = -1;   /* OCTET_STRING_SIZE_1 */
static int hf_epsli_trafficAggregateDescription = -1;  /* OCTET_STRING */
static int hf_epsli_failedTAUReason = -1;         /* OCTET_STRING_SIZE_1 */
static int hf_epsli_failedEUTRANAttachReason = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_servingMMEaddress = -1;       /* OCTET_STRING */
static int hf_epsli_bearerDeactivationType = -1;  /* TypeOfBearer */
static int hf_epsli_bearerDeactivationCause = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_ePSlocationOfTheTarget = -1;  /* EPSLocation */
static int hf_epsli_pDNType = -1;                 /* OCTET_STRING_SIZE_1 */
static int hf_epsli_requestType = -1;             /* OCTET_STRING_SIZE_1 */
static int hf_epsli_uEReqPDNConnFailReason = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_extendedHandoverIndication = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_uELocalIPAddress = -1;        /* OCTET_STRING */
static int hf_epsli_uEUdpPort = -1;               /* OCTET_STRING_SIZE_2 */
static int hf_epsli_tWANIdentifier = -1;          /* OCTET_STRING */
static int hf_epsli_tWANIdentifierTimestamp = -1;  /* OCTET_STRING_SIZE_4 */
static int hf_epsli_proSeRemoteUeContextConnected = -1;  /* RemoteUeContextConnected */
static int hf_epsli_proSeRemoteUeContextDisconnected = -1;  /* RemoteUeContextDisconnected */
static int hf_epsli_secondaryRATUsageIndication = -1;  /* NULL */
static int hf_epsli_fullyQualifiedTeid = -1;      /* OCTET_STRING */
static int hf_epsli_userLocationInfo = -1;        /* OCTET_STRING_SIZE_1_39 */
static int hf_epsli_olduserLocationInfo = -1;     /* OCTET_STRING_SIZE_1_39 */
static int hf_epsli_lastVisitedTAI = -1;          /* OCTET_STRING_SIZE_1_5 */
static int hf_epsli_tAIlist = -1;                 /* OCTET_STRING_SIZE_7_97 */
static int hf_epsli_threeGPP2Bsid = -1;           /* OCTET_STRING_SIZE_1_12 */
static int hf_epsli_uELocationTimestamp_01 = -1;  /* T_uELocationTimestamp_01 */
static int hf_epsli_ueToNetwork = -1;             /* OCTET_STRING_SIZE_1_251 */
static int hf_epsli_networkToUe = -1;             /* OCTET_STRING_SIZE_1_251 */
static int hf_epsli_RemoteUeContextConnected_item = -1;  /* RemoteUEContext */
static int hf_epsli_remoteUserID = -1;            /* RemoteUserID */
static int hf_epsli_remoteUEIPInformation = -1;   /* RemoteUEIPInformation */
static int hf_epsli_lifetime = -1;                /* INTEGER_0_65535 */
static int hf_epsli_accessTechnologyType = -1;    /* OCTET_STRING_SIZE_4 */
static int hf_epsli_iPv6HomeNetworkPrefix = -1;   /* OCTET_STRING_SIZE_20 */
static int hf_epsli_protConfigurationOption = -1;  /* OCTET_STRING */
static int hf_epsli_handoverIndication_01 = -1;   /* OCTET_STRING_SIZE_4 */
static int hf_epsli_status = -1;                  /* INTEGER_0_255 */
static int hf_epsli_revocationTrigger = -1;       /* INTEGER_0_255 */
static int hf_epsli_iPv4HomeAddress = -1;         /* OCTET_STRING_SIZE_4 */
static int hf_epsli_iPv6careOfAddress = -1;       /* OCTET_STRING */
static int hf_epsli_iPv4careOfAddress = -1;       /* OCTET_STRING */
static int hf_epsli_servingNetwork = -1;          /* OCTET_STRING_SIZE_3 */
static int hf_epsli_dHCPv4AddressAllocationInd = -1;  /* OCTET_STRING_SIZE_1 */
static int hf_epsli_requestedIPv6HomePrefix = -1;  /* OCTET_STRING_SIZE_25 */
static int hf_epsli_homeAddress = -1;             /* OCTET_STRING_SIZE_8 */
static int hf_epsli_iPv4careOfAddress_01 = -1;    /* OCTET_STRING_SIZE_8 */
static int hf_epsli_iPv6careOfAddress_01 = -1;    /* OCTET_STRING_SIZE_16 */
static int hf_epsli_hSS_AAA_address = -1;         /* OCTET_STRING */
static int hf_epsli_targetPDN_GW_Address = -1;    /* OCTET_STRING */
static int hf_epsli_homeAddress_01 = -1;          /* OCTET_STRING_SIZE_4 */
static int hf_epsli_careOfAddress = -1;           /* OCTET_STRING_SIZE_4 */
static int hf_epsli_homeAgentAddress = -1;        /* OCTET_STRING_SIZE_4 */
static int hf_epsli_code = -1;                    /* INTEGER_0_255 */
static int hf_epsli_foreignDomainAddress = -1;    /* OCTET_STRING_SIZE_4 */
static int hf_epsli_MediaDecryption_info_item = -1;  /* CCKeyInfo */
static int hf_epsli_cCCSID = -1;                  /* OCTET_STRING */
static int hf_epsli_cCDecKey = -1;                /* OCTET_STRING */
static int hf_epsli_cCSalt = -1;                  /* OCTET_STRING */
static int hf_epsli_packetDataHeader = -1;        /* PacketDataHeaderReport */
static int hf_epsli_packetDataSummary = -1;       /* PacketDataSummaryReport */
static int hf_epsli_packetDataHeaderMapped = -1;  /* PacketDataHeaderMapped */
static int hf_epsli_packetDataHeaderCopy = -1;    /* PacketDataHeaderCopy */
static int hf_epsli_sourceIPAddress = -1;         /* IPAddress */
static int hf_epsli_sourcePortNumber = -1;        /* INTEGER_0_65535 */
static int hf_epsli_destinationIPAddress = -1;    /* IPAddress */
static int hf_epsli_destinationPortNumber = -1;   /* INTEGER_0_65535 */
static int hf_epsli_transportProtocol = -1;       /* INTEGER */
static int hf_epsli_packetsize = -1;              /* INTEGER */
static int hf_epsli_flowLabel = -1;               /* INTEGER */
static int hf_epsli_packetCount = -1;             /* INTEGER */
static int hf_epsli_direction_01 = -1;            /* TPDU_direction */
static int hf_epsli_headerCopy = -1;              /* OCTET_STRING */
static int hf_epsli_PacketDataSummaryReport_item = -1;  /* PacketFlowSummary */
static int hf_epsli_summaryPeriod = -1;           /* ReportInterval */
static int hf_epsli_sumOfPacketSizes = -1;        /* INTEGER */
static int hf_epsli_packetDataSummaryReason = -1;  /* ReportReason */
static int hf_epsli_firstPacketTimeStamp = -1;    /* TimeStamp */
static int hf_epsli_lastPacketTimeStamp = -1;     /* TimeStamp */
static int hf_epsli_rfc2868ValueField = -1;       /* OCTET_STRING */
static int hf_epsli_nativeIPSec = -1;             /* NULL */
static int hf_epsli_new_MSISDN = -1;              /* PartyInformation */
static int hf_epsli_new_A_MSISDN = -1;            /* PartyInformation */
static int hf_epsli_old_MSISDN = -1;              /* PartyInformation */
static int hf_epsli_old_A_MSISDN = -1;            /* PartyInformation */
static int hf_epsli_new_IMSI = -1;                /* PartyInformation */
static int hf_epsli_old_IMSI = -1;                /* PartyInformation */
static int hf_epsli_new_IMEI = -1;                /* PartyInformation */
static int hf_epsli_old_IMEI = -1;                /* PartyInformation */
static int hf_epsli_new_IMPI = -1;                /* PartyInformation */
static int hf_epsli_old_IMPI = -1;                /* PartyInformation */
static int hf_epsli_new_SIP_URI = -1;             /* PartyInformation */
static int hf_epsli_old_SIP_URI = -1;             /* PartyInformation */
static int hf_epsli_new_TEL_URI = -1;             /* PartyInformation */
static int hf_epsli_old_TEL_URI = -1;             /* PartyInformation */
static int hf_epsli_current_Serving_MME_Address = -1;  /* DataNodeIdentifier */
static int hf_epsli_previous_Serving_System_Identifier = -1;  /* OCTET_STRING */
static int hf_epsli_previous_Serving_MME_Address = -1;  /* DataNodeIdentifier */
static int hf_epsli_reason_CodeAVP = -1;          /* INTEGER */
static int hf_epsli_server_AssignmentType = -1;   /* INTEGER */
static int hf_epsli_cipher = -1;                  /* UTF8String */
static int hf_epsli_cryptoContext = -1;           /* UTF8String */
static int hf_epsli_key = -1;                     /* UTF8String */
static int hf_epsli_keyEncoding = -1;             /* UTF8String */
static int hf_epsli_salt = -1;                    /* UTF8String */
static int hf_epsli_pTCOther = -1;                /* UTF8String */
static int hf_epsli_abandonCause = -1;            /* UTF8String */
static int hf_epsli_accessPolicyFailure = -1;     /* UTF8String */
static int hf_epsli_accessPolicyType = -1;        /* AccessPolicyType */
static int hf_epsli_alertIndicator = -1;          /* AlertIndicator */
static int hf_epsli_associatePresenceStatus = -1;  /* AssociatePresenceStatus */
static int hf_epsli_bearer_capability = -1;       /* UTF8String */
static int hf_epsli_broadcastIndicator = -1;      /* BOOLEAN */
static int hf_epsli_contactID = -1;               /* UTF8String */
static int hf_epsli_emergency = -1;               /* Emergency */
static int hf_epsli_emergencyGroupState = -1;     /* EmergencyGroupState */
static int hf_epsli_pTCType = -1;                 /* PTCType */
static int hf_epsli_failureCode = -1;             /* UTF8String */
static int hf_epsli_floorActivity = -1;           /* FloorActivity */
static int hf_epsli_floorSpeakerID = -1;          /* PTCAddress */
static int hf_epsli_groupAdSender = -1;           /* UTF8String */
static int hf_epsli_groupAuthRule = -1;           /* GroupAuthRule */
static int hf_epsli_groupCharacteristics = -1;    /* UTF8String */
static int hf_epsli_holdRetrieveInd = -1;         /* BOOLEAN */
static int hf_epsli_imminentPerilInd = -1;        /* ImminentPerilInd */
static int hf_epsli_implicitFloorReq = -1;        /* ImplicitFloorReq */
static int hf_epsli_initiationCause = -1;         /* InitiationCause */
static int hf_epsli_invitationCause = -1;         /* UTF8String */
static int hf_epsli_iPAPartyID = -1;              /* UTF8String */
static int hf_epsli_iPADirection = -1;            /* IPADirection */
static int hf_epsli_listManagementAction = -1;    /* ListManagementAction */
static int hf_epsli_listManagementFailure = -1;   /* UTF8String */
static int hf_epsli_listManagementType = -1;      /* ListManagementType */
static int hf_epsli_maxTBTime = -1;               /* UTF8String */
static int hf_epsli_mCPTTGroupID = -1;            /* UTF8String */
static int hf_epsli_mCPTTID = -1;                 /* UTF8String */
static int hf_epsli_mCPTTInd = -1;                /* BOOLEAN */
static int hf_epsli_mCPTTOrganizationName = -1;   /* UTF8String */
static int hf_epsli_mediaStreamAvail = -1;        /* BOOLEAN */
static int hf_epsli_priority_Level = -1;          /* Priority_Level */
static int hf_epsli_preEstSessionID = -1;         /* UTF8String */
static int hf_epsli_preEstStatus = -1;            /* PreEstStatus */
static int hf_epsli_pTCGroupID = -1;              /* UTF8String */
static int hf_epsli_pTCIDList = -1;               /* UTF8String */
static int hf_epsli_pTCMediaCapability = -1;      /* UTF8String */
static int hf_epsli_pTCOriginatingId = -1;        /* UTF8String */
static int hf_epsli_pTCParticipants = -1;         /* UTF8String */
static int hf_epsli_pTCParty = -1;                /* UTF8String */
static int hf_epsli_pTCPartyDrop = -1;            /* UTF8String */
static int hf_epsli_pTCSessionInfo = -1;          /* UTF8String */
static int hf_epsli_pTCServerURI = -1;            /* UTF8String */
static int hf_epsli_pTCUserAccessPolicy = -1;     /* UTF8String */
static int hf_epsli_pTCAddress = -1;              /* PTCAddress */
static int hf_epsli_queuedFloorControl = -1;      /* BOOLEAN */
static int hf_epsli_queuedPosition = -1;          /* UTF8String */
static int hf_epsli_registrationRequest = -1;     /* RegistrationRequest */
static int hf_epsli_registrationOutcome = -1;     /* RegistrationOutcome */
static int hf_epsli_retrieveID = -1;              /* UTF8String */
static int hf_epsli_rTPSetting = -1;              /* RTPSetting */
static int hf_epsli_talkBurstPriority = -1;       /* Priority_Level */
static int hf_epsli_talkBurstReason = -1;         /* Talk_burst_reason_code */
static int hf_epsli_talkburstControlSetting = -1;  /* TalkburstControlSetting */
static int hf_epsli_targetPresenceStatus = -1;    /* UTF8String */
static int hf_epsli_port_Number = -1;             /* INTEGER_0_65535 */
static int hf_epsli_userAccessPolicyAttempt = -1;  /* BOOLEAN */
static int hf_epsli_groupAuthorizationRulesAttempt = -1;  /* BOOLEAN */
static int hf_epsli_userAccessPolicyQuery = -1;   /* BOOLEAN */
static int hf_epsli_groupAuthorizationRulesQuery = -1;  /* BOOLEAN */
static int hf_epsli_userAccessPolicyResult = -1;  /* UTF8String */
static int hf_epsli_groupAuthorizationRulesResult = -1;  /* UTF8String */
static int hf_epsli_presenceID = -1;              /* UTF8String */
static int hf_epsli_presenceType = -1;            /* PresenceType */
static int hf_epsli_presenceStatus = -1;          /* BOOLEAN */
static int hf_epsli_clientEmergencyState = -1;    /* T_clientEmergencyState */
static int hf_epsli_groupEmergencyState = -1;     /* T_groupEmergencyState */
static int hf_epsli_tBCP_Request = -1;            /* BOOLEAN */
static int hf_epsli_tBCP_Granted = -1;            /* BOOLEAN */
static int hf_epsli_tBCP_Deny = -1;               /* BOOLEAN */
static int hf_epsli_tBCP_Queued = -1;             /* BOOLEAN */
static int hf_epsli_tBCP_Release = -1;            /* BOOLEAN */
static int hf_epsli_tBCP_Revoke = -1;             /* BOOLEAN */
static int hf_epsli_tBCP_Taken = -1;              /* BOOLEAN */
static int hf_epsli_tBCP_Idle = -1;               /* BOOLEAN */
static int hf_epsli_uri = -1;                     /* UTF8String */
static int hf_epsli_privacy_setting = -1;         /* BOOLEAN */
static int hf_epsli_privacy_alias = -1;           /* VisibleString */
static int hf_epsli_nickname = -1;                /* UTF8String */
static int hf_epsli_ip_address = -1;              /* IPAddress */
static int hf_epsli_port_number = -1;             /* Port_Number */
static int hf_epsli_talk_BurstControlProtocol = -1;  /* UTF8String */
static int hf_epsli_talk_Burst_parameters = -1;   /* T_talk_Burst_parameters */
static int hf_epsli_talk_Burst_parameters_item = -1;  /* VisibleString */
static int hf_epsli_tBCP_PortNumber = -1;         /* INTEGER_0_65535 */
static int hf_epsli_uLIC_header = -1;             /* ULIC_header */
static int hf_epsli_payload = -1;                 /* T_payload */
static int hf_epsli_hi3DomainId = -1;             /* OBJECT_IDENTIFIER */
static int hf_epsli_lIID = -1;                    /* LawfulInterceptionIdentifier */
static int hf_epsli_correlation_Number = -1;      /* EPSCorrelationNumber */
static int hf_epsli_sequence_number = -1;         /* INTEGER_0_65535 */
static int hf_epsli_t_PDU_direction = -1;         /* TPDU_direction */
static int hf_epsli_national_HI3_ASN1parameters = -1;  /* National_HI3_ASN1parameters */

/*--- End of included file: packet-epsli-hf.c ---*/
#line 39 "./asn1/epsli/packet-epsli-template.c"


/*--- Included file: packet-epsli-ett.c ---*/
#line 1 "./asn1/epsli/packet-epsli-ett.c"
static gint ett_epsli_TimeStamp = -1;
static gint ett_epsli_LocalTimeStamp = -1;
static gint ett_epsli_Network_Identifier = -1;
static gint ett_epsli_Network_Element_Identifier = -1;
static gint ett_epsli_National_Parameters = -1;
static gint ett_epsli_National_HI2_ASN1parameters = -1;
static gint ett_epsli_DataNodeAddress = -1;
static gint ett_epsli_IPAddress = -1;
static gint ett_epsli_IP_value = -1;
static gint ett_epsli_CivicAddress = -1;
static gint ett_epsli_SET_OF_DetailedCivicAddress = -1;
static gint ett_epsli_DetailedCivicAddress = -1;
static gint ett_epsli_ExtendedLocParameters = -1;
static gint ett_epsli_T_mapData = -1;
static gint ett_epsli_T_altitude = -1;
static gint ett_epsli_T_motionStateList = -1;
static gint ett_epsli_T_secondaryMotionState = -1;
static gint ett_epsli_T_floor = -1;
static gint ett_epsli_EpsIRIContent = -1;
static gint ett_epsli_IRI_Parameters = -1;
static gint ett_epsli_SET_SIZE_1_10_OF_PartyInformation = -1;
static gint ett_epsli_SEQUENCE_OF_PANI_Header_Info = -1;
static gint ett_epsli_SEQUENCE_OF_PartyInformation = -1;
static gint ett_epsli_SEQUENCE_OF_AdditionalCellID = -1;
static gint ett_epsli_DataNodeIdentifier = -1;
static gint ett_epsli_PANI_Header_Info = -1;
static gint ett_epsli_PANI_Location = -1;
static gint ett_epsli_PartyInformation = -1;
static gint ett_epsli_T_partyIdentity = -1;
static gint ett_epsli_Location = -1;
static gint ett_epsli_T_uELocationTimestamp = -1;
static gint ett_epsli_AdditionalCellID = -1;
static gint ett_epsli_PLMNID = -1;
static gint ett_epsli_NCGI = -1;
static gint ett_epsli_GSMLocation = -1;
static gint ett_epsli_T_geoCoordinates = -1;
static gint ett_epsli_T_utmCoordinates = -1;
static gint ett_epsli_T_utmRefCoordinates = -1;
static gint ett_epsli_UMTSLocation = -1;
static gint ett_epsli_GeographicalCoordinates = -1;
static gint ett_epsli_GA_Point = -1;
static gint ett_epsli_GA_PointWithUnCertainty = -1;
static gint ett_epsli_GA_Polygon = -1;
static gint ett_epsli_GA_Polygon_item = -1;
static gint ett_epsli_SMS_report = -1;
static gint ett_epsli_T_sMS_Contents = -1;
static gint ett_epsli_CorrelationValues = -1;
static gint ett_epsli_T_both_IRI_CC = -1;
static gint ett_epsli_IMS_VoIP_Correlation = -1;
static gint ett_epsli_IMS_VoIP_Correlation_item = -1;
static gint ett_epsli_IRI_to_CC_Correlation = -1;
static gint ett_epsli_T_cc = -1;
static gint ett_epsli_Services_Data_Information = -1;
static gint ett_epsli_GPRS_parameters = -1;
static gint ett_epsli_UmtsQos = -1;
static gint ett_epsli_EPS_GTPV2_SpecificParameters = -1;
static gint ett_epsli_EPSLocation = -1;
static gint ett_epsli_T_uELocationTimestamp_01 = -1;
static gint ett_epsli_ProtConfigOptions = -1;
static gint ett_epsli_RemoteUeContextConnected = -1;
static gint ett_epsli_RemoteUEContext = -1;
static gint ett_epsli_EPS_PMIP_SpecificParameters = -1;
static gint ett_epsli_EPS_DSMIP_SpecificParameters = -1;
static gint ett_epsli_EPS_MIP_SpecificParameters = -1;
static gint ett_epsli_MediaDecryption_info = -1;
static gint ett_epsli_CCKeyInfo = -1;
static gint ett_epsli_PacketDataHeaderInformation = -1;
static gint ett_epsli_PacketDataHeaderReport = -1;
static gint ett_epsli_PacketDataHeaderMapped = -1;
static gint ett_epsli_PacketDataHeaderCopy = -1;
static gint ett_epsli_PacketDataSummaryReport = -1;
static gint ett_epsli_PacketFlowSummary = -1;
static gint ett_epsli_ReportInterval = -1;
static gint ett_epsli_TunnelProtocol = -1;
static gint ett_epsli_Change_Of_Target_Identity = -1;
static gint ett_epsli_Current_Previous_Systems = -1;
static gint ett_epsli_DeregistrationReason = -1;
static gint ett_epsli_PTCEncryptionInfo = -1;
static gint ett_epsli_PTC = -1;
static gint ett_epsli_AccessPolicyType = -1;
static gint ett_epsli_AssociatePresenceStatus = -1;
static gint ett_epsli_EmergencyGroupState = -1;
static gint ett_epsli_FloorActivity = -1;
static gint ett_epsli_PTCAddress = -1;
static gint ett_epsli_RTPSetting = -1;
static gint ett_epsli_TalkburstControlSetting = -1;
static gint ett_epsli_T_talk_Burst_parameters = -1;
static gint ett_epsli_CC_PDU = -1;
static gint ett_epsli_ULIC_header = -1;
static gint ett_epsli_National_HI3_ASN1parameters = -1;

/*--- End of included file: packet-epsli-ett.c ---*/
#line 41 "./asn1/epsli/packet-epsli-template.c"


/*--- Included file: packet-epsli-fn.c ---*/
#line 1 "./asn1/epsli/packet-epsli-fn.c"


static int
dissect_epsli_LawfulInterceptionIdentifier(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_GeneralizedTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_GeneralizedTime(implicit_tag, actx, tree, tvb, offset, hf_index);

  return offset;
}


static const value_string epsli_T_winterSummerIndication_vals[] = {
  {   0, "notProvided" },
  {   1, "winterTime" },
  {   2, "summerTime" },
  { 0, NULL }
};


static int
dissect_epsli_T_winterSummerIndication(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t LocalTimeStamp_sequence[] = {
  { &hf_epsli_generalizedTime, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_GeneralizedTime },
  { &hf_epsli_winterSummerIndication, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_T_winterSummerIndication },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_LocalTimeStamp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   LocalTimeStamp_sequence, hf_index, ett_epsli_LocalTimeStamp);

  return offset;
}



static int
dissect_epsli_UTCTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_UTCTime(implicit_tag, actx, tree, tvb, offset, hf_index, NULL, NULL);

  return offset;
}


static const value_string epsli_TimeStamp_vals[] = {
  {   0, "localTime" },
  {   1, "utcTime" },
  { 0, NULL }
};

static const ber_choice_t TimeStamp_choice[] = {
  {   0, &hf_epsli_localTime     , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_LocalTimeStamp },
  {   1, &hf_epsli_utcTime       , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_UTCTime },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_TimeStamp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 TimeStamp_choice, hf_index, ett_epsli_TimeStamp,
                                 NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_5(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_25(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_T_iP_type_vals[] = {
  {   0, "iPV4" },
  {   1, "iPV6" },
  { 0, NULL }
};


static int
dissect_epsli_T_iP_type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_4_16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_IA5String_SIZE_7_45(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_IA5String,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const value_string epsli_IP_value_vals[] = {
  {   1, "iPBinaryAddress" },
  {   2, "iPTextAddress" },
  { 0, NULL }
};

static const ber_choice_t IP_value_choice[] = {
  {   1, &hf_epsli_iPBinaryAddress, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4_16 },
  {   2, &hf_epsli_iPTextAddress , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_IA5String_SIZE_7_45 },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_IP_value(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 IP_value_choice, hf_index, ett_epsli_IP_value,
                                 NULL);

  return offset;
}


static const value_string epsli_T_iP_assignment_vals[] = {
  {   1, "static" },
  {   2, "dynamic" },
  {   3, "notKnown" },
  { 0, NULL }
};


static int
dissect_epsli_T_iP_assignment(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t IPAddress_sequence[] = {
  { &hf_epsli_iP_type       , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_T_iP_type },
  { &hf_epsli_iP_value      , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_IP_value },
  { &hf_epsli_iP_assignment , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_iP_assignment },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_IPAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   IPAddress_sequence, hf_index, ett_epsli_IPAddress);

  return offset;
}


static const value_string epsli_Network_Element_Identifier_vals[] = {
  {   1, "e164-Format" },
  {   2, "x25-Format" },
  {   3, "iP-Format" },
  {   4, "dNS-Format" },
  {   5, "iP-Address" },
  { 0, NULL }
};

static const ber_choice_t Network_Element_Identifier_choice[] = {
  {   1, &hf_epsli_e164_Format   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  {   2, &hf_epsli_x25_Format    , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  {   3, &hf_epsli_iP_Format     , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  {   4, &hf_epsli_dNS_Format    , BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  {   5, &hf_epsli_iP_Address    , BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Network_Element_Identifier(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 Network_Element_Identifier_choice, hf_index, ett_epsli_Network_Element_Identifier,
                                 NULL);

  return offset;
}


static const ber_sequence_t Network_Identifier_sequence[] = {
  { &hf_epsli_operator_Identifier, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_5 },
  { &hf_epsli_network_Element_Identifier, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_Network_Element_Identifier },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Network_Identifier(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   Network_Identifier_sequence, hf_index, ett_epsli_Network_Identifier);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_256(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t National_Parameters_set_of[1] = {
  { &hf_epsli_National_Parameters_item, BER_CLASS_UNI, BER_UNI_TAG_OCTETSTRING, BER_FLAGS_NOOWNTAG, dissect_epsli_OCTET_STRING_SIZE_1_256 },
};

static int
dissect_epsli_National_Parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 National_Parameters_set_of, hf_index, ett_epsli_National_Parameters);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_2(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t National_HI2_ASN1parameters_sequence[] = {
  { &hf_epsli_countryCode   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_2 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_National_HI2_ASN1parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   National_HI2_ASN1parameters_sequence, hf_index, ett_epsli_National_HI2_ASN1parameters);

  return offset;
}



static int
dissect_epsli_X25Address(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_DataNodeAddress_vals[] = {
  {   1, "ipAddress" },
  {   2, "x25Address" },
  { 0, NULL }
};

static const ber_choice_t DataNodeAddress_choice[] = {
  {   1, &hf_epsli_ipAddress     , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  {   2, &hf_epsli_x25Address    , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_X25Address },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_DataNodeAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 DataNodeAddress_choice, hf_index, ett_epsli_DataNodeAddress,
                                 NULL);

  return offset;
}



static int
dissect_epsli_UTF8String(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_UTF8String,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t DetailedCivicAddress_sequence[] = {
  { &hf_epsli_building      , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_room          , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_placeType     , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_postalCommunityName, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_additionalCode, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_seat          , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_primaryRoad   , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_primaryRoadDirection, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_trailingStreetSuffix, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_streetSuffix  , BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_houseNumber   , BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_houseNumberSuffix, BER_CLASS_CON, 12, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_landmarkAddress, BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_additionalLocation, BER_CLASS_CON, 114, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_name          , BER_CLASS_CON, 15, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_floor         , BER_CLASS_CON, 16, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_primaryStreet , BER_CLASS_CON, 17, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_primaryStreetDirection, BER_CLASS_CON, 18, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_roadSection   , BER_CLASS_CON, 19, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_roadBranch    , BER_CLASS_CON, 20, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_roadSubBranch , BER_CLASS_CON, 21, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_roadPreModifier, BER_CLASS_CON, 22, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_roadPostModifier, BER_CLASS_CON, 23, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_postalCode    , BER_CLASS_CON, 24, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_town          , BER_CLASS_CON, 25, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_county        , BER_CLASS_CON, 26, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_country       , BER_CLASS_CON, 27, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_language      , BER_CLASS_CON, 28, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_DetailedCivicAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   DetailedCivicAddress_sequence, hf_index, ett_epsli_DetailedCivicAddress);

  return offset;
}


static const ber_sequence_t SET_OF_DetailedCivicAddress_set_of[1] = {
  { &hf_epsli_detailedCivicAddress_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_DetailedCivicAddress },
};

static int
dissect_epsli_SET_OF_DetailedCivicAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 SET_OF_DetailedCivicAddress_set_of, hf_index, ett_epsli_SET_OF_DetailedCivicAddress);

  return offset;
}



static int
dissect_epsli_XmlCivicAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_UTF8String,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const value_string epsli_CivicAddress_vals[] = {
  {   0, "detailedCivicAddress" },
  {   1, "xmlCivicAddress" },
  { 0, NULL }
};

static const ber_choice_t CivicAddress_choice[] = {
  {   0, &hf_epsli_detailedCivicAddress, BER_CLASS_UNI, BER_UNI_TAG_SET, BER_FLAGS_NOOWNTAG, dissect_epsli_SET_OF_DetailedCivicAddress },
  {   1, &hf_epsli_xmlCivicAddress, BER_CLASS_UNI, BER_UNI_TAG_UTF8String, BER_FLAGS_NOOWNTAG, dissect_epsli_XmlCivicAddress },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_CivicAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 CivicAddress_choice, hf_index, ett_epsli_CivicAddress,
                                 NULL);

  return offset;
}



static int
dissect_epsli_PrintableString(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const value_string epsli_T_mapData_vals[] = {
  {   0, "base64Map" },
  {   1, "url" },
  { 0, NULL }
};

static const ber_choice_t T_mapData_choice[] = {
  {   0, &hf_epsli_base64Map     , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  {   1, &hf_epsli_url           , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_mapData(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 T_mapData_choice, hf_index, ett_epsli_T_mapData,
                                 NULL);

  return offset;
}


static const ber_sequence_t T_altitude_sequence[] = {
  { &hf_epsli_alt           , BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString },
  { &hf_epsli_alt_uncertainty, BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_OPTIONAL|BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_altitude(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_altitude_sequence, hf_index, ett_epsli_T_altitude);

  return offset;
}



static int
dissect_epsli_BOOLEAN(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_boolean(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);

  return offset;
}


static const ber_sequence_t T_secondaryMotionState_sequence_of[1] = {
  { &hf_epsli_secondaryMotionState_item, BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString },
};

static int
dissect_epsli_T_secondaryMotionState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      T_secondaryMotionState_sequence_of, hf_index, ett_epsli_T_secondaryMotionState);

  return offset;
}


static const ber_sequence_t T_motionStateList_sequence[] = {
  { &hf_epsli_primaryMotionState, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_secondaryMotionState, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_secondaryMotionState },
  { &hf_epsli_confidence    , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_motionStateList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_motionStateList_sequence, hf_index, ett_epsli_T_motionStateList);

  return offset;
}


static const ber_sequence_t T_floor_sequence[] = {
  { &hf_epsli_floor_number  , BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString },
  { &hf_epsli_floor_number_uncertainty, BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_OPTIONAL|BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_floor(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_floor_sequence, hf_index, ett_epsli_T_floor);

  return offset;
}


static const ber_sequence_t ExtendedLocParameters_sequence[] = {
  { &hf_epsli_posMethod     , BER_CLASS_CON, 0, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_mapData       , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_mapData },
  { &hf_epsli_altitude      , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_altitude },
  { &hf_epsli_speed         , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_direction     , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_level_conf    , BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_qOS_not_met   , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_motionStateList, BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_motionStateList },
  { &hf_epsli_floor_01      , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_floor },
  { &hf_epsli_additional_info, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_lALS_rawMLPPosData, BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_ExtendedLocParameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ExtendedLocParameters_sequence, hf_index, ett_epsli_ExtendedLocParameters);

  return offset;
}



static int
dissect_epsli_LocationErrorCode(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_epsli_NULL(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_null(implicit_tag, actx, tree, tvb, offset, hf_index);

  return offset;
}



static int
dissect_epsli_OBJECT_IDENTIFIER(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_object_identifier(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);

  return offset;
}


static const value_string epsli_T_initiator_vals[] = {
  {   0, "not-Available" },
  {   1, "originating-Target" },
  {   2, "terminating-Target" },
  { 0, NULL }
};


static int
dissect_epsli_T_initiator(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_GlobalCellID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_Rai(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_7_10(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_8_11(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const value_string epsli_MapDatum_vals[] = {
  {   0, "wGS84" },
  {   1, "wGS72" },
  {   2, "eD50" },
  { 0, NULL }
};


static int
dissect_epsli_MapDatum(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_INTEGER_0_359(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const ber_sequence_t T_geoCoordinates_sequence[] = {
  { &hf_epsli_latitude      , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_7_10 },
  { &hf_epsli_longitude     , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_8_11 },
  { &hf_epsli_mapDatum      , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_MapDatum },
  { &hf_epsli_azimuth       , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_359 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_geoCoordinates(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_geoCoordinates_sequence, hf_index, ett_epsli_T_geoCoordinates);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_10(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_7(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t T_utmCoordinates_sequence[] = {
  { &hf_epsli_utm_East      , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_10 },
  { &hf_epsli_utm_North     , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_7 },
  { &hf_epsli_mapDatum      , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_MapDatum },
  { &hf_epsli_azimuth       , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_359 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_utmCoordinates(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_utmCoordinates_sequence, hf_index, ett_epsli_T_utmCoordinates);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_13(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t T_utmRefCoordinates_sequence[] = {
  { &hf_epsli_utmref_string , BER_CLASS_UNI, BER_UNI_TAG_PrintableString, BER_FLAGS_NOOWNTAG, dissect_epsli_PrintableString_SIZE_13 },
  { &hf_epsli_mapDatum      , BER_CLASS_UNI, BER_UNI_TAG_ENUMERATED, BER_FLAGS_OPTIONAL|BER_FLAGS_NOOWNTAG, dissect_epsli_MapDatum },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_utmRefCoordinates(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_utmRefCoordinates_sequence, hf_index, ett_epsli_T_utmRefCoordinates);

  return offset;
}



static int
dissect_epsli_OCTET_STRING(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_GSMLocation_vals[] = {
  {   1, "geoCoordinates" },
  {   2, "utmCoordinates" },
  {   3, "utmRefCoordinates" },
  {   4, "wGS84Coordinates" },
  { 0, NULL }
};

static const ber_choice_t GSMLocation_choice[] = {
  {   1, &hf_epsli_geoCoordinates, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_T_geoCoordinates },
  {   2, &hf_epsli_utmCoordinates, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_T_utmCoordinates },
  {   3, &hf_epsli_utmRefCoordinates, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_T_utmRefCoordinates },
  {   4, &hf_epsli_wGS84Coordinates, BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GSMLocation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 GSMLocation_choice, hf_index, ett_epsli_GSMLocation,
                                 NULL);

  return offset;
}


static const value_string epsli_T_latitudeSign_vals[] = {
  {   0, "north" },
  {   1, "south" },
  { 0, NULL }
};


static int
dissect_epsli_T_latitudeSign(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_INTEGER_0_8388607(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_epsli_INTEGER_M8388608_8388607(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const ber_sequence_t GeographicalCoordinates_sequence[] = {
  { &hf_epsli_latitudeSign  , BER_CLASS_UNI, BER_UNI_TAG_ENUMERATED, BER_FLAGS_NOOWNTAG, dissect_epsli_T_latitudeSign },
  { &hf_epsli_latitude_01   , BER_CLASS_UNI, BER_UNI_TAG_INTEGER, BER_FLAGS_NOOWNTAG, dissect_epsli_INTEGER_0_8388607 },
  { &hf_epsli_longitude_01  , BER_CLASS_UNI, BER_UNI_TAG_INTEGER, BER_FLAGS_NOOWNTAG, dissect_epsli_INTEGER_M8388608_8388607 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GeographicalCoordinates(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GeographicalCoordinates_sequence, hf_index, ett_epsli_GeographicalCoordinates);

  return offset;
}


static const ber_sequence_t GA_Point_sequence[] = {
  { &hf_epsli_geographicalCoordinates, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_GeographicalCoordinates },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GA_Point(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GA_Point_sequence, hf_index, ett_epsli_GA_Point);

  return offset;
}



static int
dissect_epsli_INTEGER_0_127(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const ber_sequence_t GA_PointWithUnCertainty_sequence[] = {
  { &hf_epsli_geographicalCoordinates, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_GeographicalCoordinates },
  { &hf_epsli_uncertaintyCode, BER_CLASS_UNI, BER_UNI_TAG_INTEGER, BER_FLAGS_NOOWNTAG, dissect_epsli_INTEGER_0_127 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GA_PointWithUnCertainty(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GA_PointWithUnCertainty_sequence, hf_index, ett_epsli_GA_PointWithUnCertainty);

  return offset;
}


static const ber_sequence_t GA_Polygon_item_sequence[] = {
  { &hf_epsli_geographicalCoordinates, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_GeographicalCoordinates },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GA_Polygon_item(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GA_Polygon_item_sequence, hf_index, ett_epsli_GA_Polygon_item);

  return offset;
}


static const ber_sequence_t GA_Polygon_sequence_of[1] = {
  { &hf_epsli_GA_Polygon_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_GA_Polygon_item },
};

static int
dissect_epsli_GA_Polygon(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      GA_Polygon_sequence_of, hf_index, ett_epsli_GA_Polygon);

  return offset;
}


static const value_string epsli_UMTSLocation_vals[] = {
  {   1, "point" },
  {   2, "pointWithUnCertainty" },
  {   3, "polygon" },
  { 0, NULL }
};

static const ber_choice_t UMTSLocation_choice[] = {
  {   1, &hf_epsli_point         , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_GA_Point },
  {   2, &hf_epsli_pointWithUnCertainty, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_GA_PointWithUnCertainty },
  {   3, &hf_epsli_polygon       , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_GA_Polygon },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_UMTSLocation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 UMTSLocation_choice, hf_index, ett_epsli_UMTSLocation,
                                 NULL);

  return offset;
}



static int
dissect_epsli_Sai(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_T_uELocationTimestamp_vals[] = {
  {   0, "timestamp" },
  {   1, "timestampUnknown" },
  { 0, NULL }
};

static const ber_choice_t T_uELocationTimestamp_choice[] = {
  {   0, &hf_epsli_timestamp     , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_TimeStamp },
  {   1, &hf_epsli_timestampUnknown, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_uELocationTimestamp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 T_uELocationTimestamp_choice, hf_index, ett_epsli_T_uELocationTimestamp,
                                 NULL);

  return offset;
}


static const ber_sequence_t Location_sequence[] = {
  { &hf_epsli_e164_Number   , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  { &hf_epsli_globalCellID  , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_GlobalCellID },
  { &hf_epsli_rAI           , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Rai },
  { &hf_epsli_gsmLocation   , BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_GSMLocation },
  { &hf_epsli_umtsLocation  , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_UMTSLocation },
  { &hf_epsli_sAI           , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Sai },
  { &hf_epsli_oldRAI        , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Rai },
  { &hf_epsli_civicAddress  , BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_CivicAddress },
  { &hf_epsli_operatorSpecificInfo, BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_uELocationTimestamp, BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_uELocationTimestamp },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Location(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   Location_sequence, hf_index, ett_epsli_Location);

  return offset;
}


static const value_string epsli_T_party_Qualifier_vals[] = {
  {   3, "gPRSorEPS-Target" },
  { 0, NULL }
};


static int
dissect_epsli_T_party_Qualifier(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_3_8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_9(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t T_partyIdentity_sequence[] = {
  { &hf_epsli_imei          , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_imsi          , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_3_8 },
  { &hf_epsli_msISDN        , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_9 },
  { &hf_epsli_e164_Format   , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_25 },
  { &hf_epsli_sip_uri       , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_tel_uri       , BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_nai           , BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_x_3GPP_Asserted_Identity, BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_xUI           , BER_CLASS_CON, 12, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_iMPI          , BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_extID         , BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_partyIdentity(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_partyIdentity_sequence, hf_index, ett_epsli_T_partyIdentity);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_100(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_2(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t GPRS_parameters_sequence[] = {
  { &hf_epsli_pDP_address_allocated_to_the_target, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_aPN           , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_100 },
  { &hf_epsli_pDP_type      , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_2 },
  { &hf_epsli_nSAPI         , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_additionalIPaddress, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_GPRS_parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GPRS_parameters_sequence, hf_index, ett_epsli_GPRS_parameters);

  return offset;
}


static const ber_sequence_t Services_Data_Information_sequence[] = {
  { &hf_epsli_gPRS_parameters, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_GPRS_parameters },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Services_Data_Information(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   Services_Data_Information_sequence, hf_index, ett_epsli_Services_Data_Information);

  return offset;
}


static const ber_sequence_t PartyInformation_sequence[] = {
  { &hf_epsli_party_Qualifier, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_T_party_Qualifier },
  { &hf_epsli_partyIdentity , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_T_partyIdentity },
  { &hf_epsli_services_Data_Information, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Services_Data_Information },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PartyInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PartyInformation_sequence, hf_index, ett_epsli_PartyInformation);

  return offset;
}


static const ber_sequence_t SET_SIZE_1_10_OF_PartyInformation_set_of[1] = {
  { &hf_epsli_partyInformation_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_PartyInformation },
};

static int
dissect_epsli_SET_SIZE_1_10_OF_PartyInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 SET_SIZE_1_10_OF_PartyInformation_set_of, hf_index, ett_epsli_SET_SIZE_1_10_OF_PartyInformation);

  return offset;
}


static const value_string epsli_T_sms_initiator_vals[] = {
  {   0, "target" },
  {   1, "server" },
  {   2, "undefined-party" },
  { 0, NULL }
};


static int
dissect_epsli_T_sms_initiator(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_T_transfer_status_vals[] = {
  {   0, "succeed-transfer" },
  {   1, "not-succeed-transfer" },
  {   2, "undefined" },
  { 0, NULL }
};


static int
dissect_epsli_T_transfer_status(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_T_other_message_vals[] = {
  {   0, "yes" },
  {   1, "no" },
  {   2, "undefined" },
  { 0, NULL }
};


static int
dissect_epsli_T_other_message(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_270(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t T_sMS_Contents_sequence[] = {
  { &hf_epsli_sms_initiator , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_T_sms_initiator },
  { &hf_epsli_transfer_status, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_transfer_status },
  { &hf_epsli_other_message , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_other_message },
  { &hf_epsli_content       , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_270 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_sMS_Contents(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_sMS_Contents_sequence, hf_index, ett_epsli_T_sMS_Contents);

  return offset;
}


static const ber_sequence_t SMS_report_sequence[] = {
  { &hf_epsli_sMS_Contents  , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_T_sMS_Contents },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_SMS_report(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SMS_report_sequence, hf_index, ett_epsli_SMS_report);

  return offset;
}



static int
dissect_epsli_EPSCorrelationNumber(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_EPSEvent_vals[] = {
  {   1, "pDPContextActivation" },
  {   2, "startOfInterceptionWithPDPContextActive" },
  {   4, "pDPContextDeactivation" },
  {   5, "gPRSAttach" },
  {   6, "gPRSDetach" },
  {  10, "locationInfoUpdate" },
  {  11, "sMS" },
  {  13, "pDPContextModification" },
  {  14, "servingSystem" },
  {  15, "startOfInterceptionWithMSAttached" },
  {  16, "e-UTRANAttach" },
  {  17, "e-UTRANDetach" },
  {  18, "bearerActivation" },
  {  19, "startOfInterceptionWithActiveBearer" },
  {  20, "bearerModification" },
  {  21, "bearerDeactivation" },
  {  22, "uERequestedBearerResourceModification" },
  {  23, "uERequestedPDNConnectivity" },
  {  24, "uERequestedPDNDisconnection" },
  {  25, "trackingAreaEpsLocationUpdate" },
  {  26, "servingEvolvedPacketSystem" },
  {  27, "pMIPAttachTunnelActivation" },
  {  28, "pMIPDetachTunnelDeactivation" },
  {  29, "startOfInterceptWithActivePMIPTunnel" },
  {  30, "pMIPPdnGwInitiatedPdnDisconnection" },
  {  31, "mIPRegistrationTunnelActivation" },
  {  32, "mIPDeregistrationTunnelDeactivation" },
  {  33, "startOfInterceptWithActiveMIPTunnel" },
  {  34, "dSMIPRegistrationTunnelActivation" },
  {  35, "dSMIPDeregistrationTunnelDeactivation" },
  {  36, "startOfInterceptWithActiveDsmipTunnel" },
  {  37, "dSMipHaSwitch" },
  {  38, "pMIPResourceAllocationDeactivation" },
  {  39, "mIPResourceAllocationDeactivation" },
  {  40, "pMIPsessionModification" },
  {  41, "startOfInterceptWithEUTRANAttachedUE" },
  {  42, "dSMIPSessionModification" },
  {  43, "packetDataHeaderInformation" },
  {  44, "hSS-Subscriber-Record-Change" },
  {  45, "registration-Termination" },
  {  46, "location-Up-Date" },
  {  47, "cancel-Location" },
  {  48, "register-Location" },
  {  49, "location-Information-Request" },
  {  50, "proSeRemoteUEReport" },
  {  51, "proSeRemoteUEStartOfCommunication" },
  {  52, "proSeRemoteUEEndOfCommunication" },
  {  53, "startOfLIwithProSeRemoteUEOngoingComm" },
  {  54, "startOfLIforProSeUEtoNWRelay" },
  {  55, "scefRequestednonIPPDNDisconnection" },
  { 0, NULL }
};


static int
dissect_epsli_EPSEvent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_GPRSOperationErrorCode(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_UmtsQos_vals[] = {
  {   1, "qosMobileRadio" },
  {   2, "qosGn" },
  { 0, NULL }
};

static const ber_choice_t UmtsQos_choice[] = {
  {   1, &hf_epsli_qosMobileRadio, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  {   2, &hf_epsli_qosGn         , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_UmtsQos(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 UmtsQos_choice, hf_index, ett_epsli_UmtsQos,
                                 NULL);

  return offset;
}


static const value_string epsli_IMSevent_vals[] = {
  {   1, "unfilteredSIPmessage" },
  {   2, "sIPheaderOnly" },
  {   3, "decryptionKeysAvailable" },
  {   4, "startOfInterceptionForIMSEstablishedSession" },
  {   5, "xCAPRequest" },
  {   6, "xCAPResponse" },
  {   7, "ccUnavailable" },
  {   8, "sMSOverIMS" },
  {   9, "servingSystem" },
  {  10, "subscriberRecordChange" },
  {  11, "registrationTermination" },
  {  12, "locationInformationRequest" },
  { 0, NULL }
};


static int
dissect_epsli_IMSevent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_20(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_5_17(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_LDIevent_vals[] = {
  {   1, "targetEntersIA" },
  {   2, "targetLeavesIA" },
  { 0, NULL }
};


static int
dissect_epsli_LDIevent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t T_cc_set_of[1] = {
  { &hf_epsli_cc_item       , BER_CLASS_UNI, BER_UNI_TAG_OCTETSTRING, BER_FLAGS_NOOWNTAG, dissect_epsli_OCTET_STRING },
};

static int
dissect_epsli_T_cc(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 T_cc_set_of, hf_index, ett_epsli_T_cc);

  return offset;
}


static const ber_sequence_t IRI_to_CC_Correlation_sequence[] = {
  { &hf_epsli_cc            , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_T_cc },
  { &hf_epsli_iri           , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_IRI_to_CC_Correlation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   IRI_to_CC_Correlation_sequence, hf_index, ett_epsli_IRI_to_CC_Correlation);

  return offset;
}



static int
dissect_epsli_IRI_to_IRI_Correlation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t T_both_IRI_CC_sequence[] = {
  { &hf_epsli_iri_CC        , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_CC_Correlation },
  { &hf_epsli_iri_IRI       , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_IRI_Correlation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_both_IRI_CC(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   T_both_IRI_CC_sequence, hf_index, ett_epsli_T_both_IRI_CC);

  return offset;
}


static const value_string epsli_CorrelationValues_vals[] = {
  {   0, "iri-to-CC" },
  {   1, "iri-to-iri" },
  {   2, "both-IRI-CC" },
  { 0, NULL }
};

static const ber_choice_t CorrelationValues_choice[] = {
  {   0, &hf_epsli_iri_to_CC     , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_CC_Correlation },
  {   1, &hf_epsli_iri_to_iri    , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_IRI_Correlation },
  {   2, &hf_epsli_both_IRI_CC   , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_T_both_IRI_CC },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_CorrelationValues(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 CorrelationValues_choice, hf_index, ett_epsli_CorrelationValues,
                                 NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_251(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t ProtConfigOptions_sequence[] = {
  { &hf_epsli_ueToNetwork   , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_251 },
  { &hf_epsli_networkToUe   , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_251 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_ProtConfigOptions(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ProtConfigOptions_sequence, hf_index, ett_epsli_ProtConfigOptions);

  return offset;
}


static const value_string epsli_TypeOfBearer_vals[] = {
  {   1, "defaultBearer" },
  {   2, "dedicatedBearer" },
  { 0, NULL }
};


static int
dissect_epsli_TypeOfBearer(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_39(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_7_97(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_1_12(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string epsli_T_uELocationTimestamp_01_vals[] = {
  {   0, "timestamp" },
  {   1, "timestampUnknown" },
  { 0, NULL }
};

static const ber_choice_t T_uELocationTimestamp_01_choice[] = {
  {   0, &hf_epsli_timestamp     , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_TimeStamp },
  {   1, &hf_epsli_timestampUnknown, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_T_uELocationTimestamp_01(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 T_uELocationTimestamp_01_choice, hf_index, ett_epsli_T_uELocationTimestamp_01,
                                 NULL);

  return offset;
}


static const ber_sequence_t EPSLocation_sequence[] = {
  { &hf_epsli_userLocationInfo, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_39 },
  { &hf_epsli_gsmLocation   , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_GSMLocation },
  { &hf_epsli_umtsLocation  , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_UMTSLocation },
  { &hf_epsli_olduserLocationInfo, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_39 },
  { &hf_epsli_lastVisitedTAI, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_5 },
  { &hf_epsli_tAIlist       , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_7_97 },
  { &hf_epsli_threeGPP2Bsid , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_12 },
  { &hf_epsli_civicAddress  , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_CivicAddress },
  { &hf_epsli_operatorSpecificInfo, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_uELocationTimestamp_01, BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_uELocationTimestamp_01 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EPSLocation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EPSLocation_sequence, hf_index, ett_epsli_EPSLocation);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_4(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_RemoteUserID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_RemoteUEIPInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t RemoteUEContext_sequence[] = {
  { &hf_epsli_remoteUserID  , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_RemoteUserID },
  { &hf_epsli_remoteUEIPInformation, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_RemoteUEIPInformation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_RemoteUEContext(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RemoteUEContext_sequence, hf_index, ett_epsli_RemoteUEContext);

  return offset;
}


static const ber_sequence_t RemoteUeContextConnected_sequence_of[1] = {
  { &hf_epsli_RemoteUeContextConnected_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_RemoteUEContext },
};

static int
dissect_epsli_RemoteUeContextConnected(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      RemoteUeContextConnected_sequence_of, hf_index, ett_epsli_RemoteUeContextConnected);

  return offset;
}



static int
dissect_epsli_RemoteUeContextDisconnected(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_epsli_RemoteUserID(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const ber_sequence_t EPS_GTPV2_SpecificParameters_sequence[] = {
  { &hf_epsli_pDNAddressAllocation, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_aPN           , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_100 },
  { &hf_epsli_protConfigOptions, BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ProtConfigOptions },
  { &hf_epsli_attachType    , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_ePSBearerIdentity, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_detachType    , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_rATType       , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_failedBearerActivationReason, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_ePSBearerQoS  , BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_bearerActivationType, BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_TypeOfBearer },
  { &hf_epsli_aPN_AMBR      , BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_procedureTransactionId, BER_CLASS_CON, 12, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_linkedEPSBearerId, BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_tFT           , BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_handoverIndication, BER_CLASS_CON, 15, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { &hf_epsli_failedBearerModReason, BER_CLASS_CON, 16, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_trafficAggregateDescription, BER_CLASS_CON, 17, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_failedTAUReason, BER_CLASS_CON, 18, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_failedEUTRANAttachReason, BER_CLASS_CON, 19, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_servingMMEaddress, BER_CLASS_CON, 20, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_bearerDeactivationType, BER_CLASS_CON, 21, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_TypeOfBearer },
  { &hf_epsli_bearerDeactivationCause, BER_CLASS_CON, 22, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_ePSlocationOfTheTarget, BER_CLASS_CON, 23, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPSLocation },
  { &hf_epsli_pDNType       , BER_CLASS_CON, 24, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_requestType   , BER_CLASS_CON, 25, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_uEReqPDNConnFailReason, BER_CLASS_CON, 26, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_extendedHandoverIndication, BER_CLASS_CON, 27, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_uLITimestamp  , BER_CLASS_CON, 28, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_uELocalIPAddress, BER_CLASS_CON, 29, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_uEUdpPort     , BER_CLASS_CON, 30, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_2 },
  { &hf_epsli_tWANIdentifier, BER_CLASS_CON, 31, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_tWANIdentifierTimestamp, BER_CLASS_CON, 32, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_proSeRemoteUeContextConnected, BER_CLASS_CON, 33, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_RemoteUeContextConnected },
  { &hf_epsli_proSeRemoteUeContextDisconnected, BER_CLASS_CON, 34, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_RemoteUeContextDisconnected },
  { &hf_epsli_secondaryRATUsageIndication, BER_CLASS_CON, 35, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { &hf_epsli_fullyQualifiedTeid, BER_CLASS_CON, 253, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EPS_GTPV2_SpecificParameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EPS_GTPV2_SpecificParameters_sequence, hf_index, ett_epsli_EPS_GTPV2_SpecificParameters);

  return offset;
}



static int
dissect_epsli_INTEGER_0_65535(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_20(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_INTEGER_0_255(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_3(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t EPS_PMIP_SpecificParameters_sequence[] = {
  { &hf_epsli_lifetime      , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_accessTechnologyType, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_aPN           , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_100 },
  { &hf_epsli_iPv6HomeNetworkPrefix, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_20 },
  { &hf_epsli_protConfigurationOption, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_handoverIndication_01, BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_status        , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_255 },
  { &hf_epsli_revocationTrigger, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_255 },
  { &hf_epsli_iPv4HomeAddress, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_iPv6careOfAddress, BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_iPv4careOfAddress, BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_servingNetwork, BER_CLASS_CON, 12, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_3 },
  { &hf_epsli_dHCPv4AddressAllocationInd, BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1 },
  { &hf_epsli_ePSlocationOfTheTarget, BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPSLocation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EPS_PMIP_SpecificParameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EPS_PMIP_SpecificParameters_sequence, hf_index, ett_epsli_EPS_PMIP_SpecificParameters);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_25(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}



static int
dissect_epsli_OCTET_STRING_SIZE_16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const ber_sequence_t EPS_DSMIP_SpecificParameters_sequence[] = {
  { &hf_epsli_lifetime      , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_requestedIPv6HomePrefix, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_25 },
  { &hf_epsli_homeAddress   , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_iPv4careOfAddress_01, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_iPv6careOfAddress_01, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_16 },
  { &hf_epsli_aPN           , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_100 },
  { &hf_epsli_status        , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_255 },
  { &hf_epsli_hSS_AAA_address, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_targetPDN_GW_Address, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EPS_DSMIP_SpecificParameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EPS_DSMIP_SpecificParameters_sequence, hf_index, ett_epsli_EPS_DSMIP_SpecificParameters);

  return offset;
}


static const ber_sequence_t EPS_MIP_SpecificParameters_sequence[] = {
  { &hf_epsli_lifetime      , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_homeAddress_01, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_careOfAddress , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_homeAgentAddress, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_code          , BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_255 },
  { &hf_epsli_foreignDomainAddress, BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EPS_MIP_SpecificParameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EPS_MIP_SpecificParameters_sequence, hf_index, ett_epsli_EPS_MIP_SpecificParameters);

  return offset;
}


static const ber_sequence_t CCKeyInfo_sequence[] = {
  { &hf_epsli_cCCSID        , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_cCDecKey      , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_cCSalt        , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_CCKeyInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   CCKeyInfo_sequence, hf_index, ett_epsli_CCKeyInfo);

  return offset;
}


static const ber_sequence_t MediaDecryption_info_sequence_of[1] = {
  { &hf_epsli_MediaDecryption_info_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_CCKeyInfo },
};

static int
dissect_epsli_MediaDecryption_info(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      MediaDecryption_info_sequence_of, hf_index, ett_epsli_MediaDecryption_info);

  return offset;
}



static int
dissect_epsli_INTEGER(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const value_string epsli_TPDU_direction_vals[] = {
  {   1, "from-target" },
  {   2, "to-target" },
  {   3, "unknown" },
  { 0, NULL }
};


static int
dissect_epsli_TPDU_direction(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t PacketDataHeaderMapped_sequence[] = {
  { &hf_epsli_sourceIPAddress, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_sourcePortNumber, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_destinationIPAddress, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_destinationPortNumber, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_transportProtocol, BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_packetsize    , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_flowLabel     , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_packetCount   , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_direction_01  , BER_CLASS_CON, 9, BER_FLAGS_IMPLTAG, dissect_epsli_TPDU_direction },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PacketDataHeaderMapped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PacketDataHeaderMapped_sequence, hf_index, ett_epsli_PacketDataHeaderMapped);

  return offset;
}


static const ber_sequence_t PacketDataHeaderCopy_sequence[] = {
  { &hf_epsli_direction_01  , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_TPDU_direction },
  { &hf_epsli_headerCopy    , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PacketDataHeaderCopy(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PacketDataHeaderCopy_sequence, hf_index, ett_epsli_PacketDataHeaderCopy);

  return offset;
}


static const value_string epsli_PacketDataHeaderReport_vals[] = {
  {   1, "packetDataHeaderMapped" },
  {   2, "packetDataHeaderCopy" },
  { 0, NULL }
};

static const ber_choice_t PacketDataHeaderReport_choice[] = {
  {   1, &hf_epsli_packetDataHeaderMapped, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PacketDataHeaderMapped },
  {   2, &hf_epsli_packetDataHeaderCopy, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PacketDataHeaderCopy },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PacketDataHeaderReport(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 PacketDataHeaderReport_choice, hf_index, ett_epsli_PacketDataHeaderReport,
                                 NULL);

  return offset;
}


static const ber_sequence_t ReportInterval_sequence[] = {
  { &hf_epsli_firstPacketTimeStamp, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TimeStamp },
  { &hf_epsli_lastPacketTimeStamp, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TimeStamp },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_ReportInterval(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ReportInterval_sequence, hf_index, ett_epsli_ReportInterval);

  return offset;
}


static const value_string epsli_ReportReason_vals[] = {
  {   0, "timerExpired" },
  {   1, "countThresholdHit" },
  {   2, "pDPComtextDeactivated" },
  {   3, "pDPContextModification" },
  {   4, "otherOrUnknown" },
  { 0, NULL }
};


static int
dissect_epsli_ReportReason(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t PacketFlowSummary_sequence[] = {
  { &hf_epsli_sourceIPAddress, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_sourcePortNumber, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_destinationIPAddress, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_destinationPortNumber, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_transportProtocol, BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_flowLabel     , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_summaryPeriod , BER_CLASS_CON, 7, BER_FLAGS_IMPLTAG, dissect_epsli_ReportInterval },
  { &hf_epsli_packetCount   , BER_CLASS_CON, 8, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_sumOfPacketSizes, BER_CLASS_CON, 9, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { &hf_epsli_packetDataSummaryReason, BER_CLASS_CON, 10, BER_FLAGS_IMPLTAG, dissect_epsli_ReportReason },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PacketFlowSummary(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PacketFlowSummary_sequence, hf_index, ett_epsli_PacketFlowSummary);

  return offset;
}


static const ber_sequence_t PacketDataSummaryReport_sequence_of[1] = {
  { &hf_epsli_PacketDataSummaryReport_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_PacketFlowSummary },
};

static int
dissect_epsli_PacketDataSummaryReport(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      PacketDataSummaryReport_sequence_of, hf_index, ett_epsli_PacketDataSummaryReport);

  return offset;
}


static const value_string epsli_PacketDataHeaderInformation_vals[] = {
  {   1, "packetDataHeader" },
  {   2, "packetDataSummary" },
  { 0, NULL }
};

static const ber_choice_t PacketDataHeaderInformation_choice[] = {
  {   1, &hf_epsli_packetDataHeader, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PacketDataHeaderReport },
  {   2, &hf_epsli_packetDataSummary, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PacketDataSummaryReport },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PacketDataHeaderInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 PacketDataHeaderInformation_choice, hf_index, ett_epsli_PacketDataHeaderInformation,
                                 NULL);

  return offset;
}


static const value_string epsli_MediaSecFailureIndication_vals[] = {
  {   0, "genericFailure" },
  { 0, NULL }
};


static int
dissect_epsli_MediaSecFailureIndication(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_HeNBLocation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_epsli_EPSLocation(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const value_string epsli_TunnelProtocol_vals[] = {
  {   0, "rfc2868ValueField" },
  {   1, "nativeIPSec" },
  { 0, NULL }
};

static const ber_choice_t TunnelProtocol_choice[] = {
  {   0, &hf_epsli_rfc2868ValueField, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  {   1, &hf_epsli_nativeIPSec   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_TunnelProtocol(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 TunnelProtocol_choice, hf_index, ett_epsli_TunnelProtocol,
                                 NULL);

  return offset;
}


static const ber_sequence_t PANI_Location_sequence[] = {
  { &hf_epsli_raw_Location  , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_location      , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Location },
  { &hf_epsli_ePSLocation   , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPSLocation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PANI_Location(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PANI_Location_sequence, hf_index, ett_epsli_PANI_Location);

  return offset;
}


static const ber_sequence_t PANI_Header_Info_sequence[] = {
  { &hf_epsli_access_Type   , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_access_Class  , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_network_Provided, BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  { &hf_epsli_pANI_Location , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PANI_Location },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PANI_Header_Info(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PANI_Header_Info_sequence, hf_index, ett_epsli_PANI_Header_Info);

  return offset;
}


static const ber_sequence_t SEQUENCE_OF_PANI_Header_Info_sequence_of[1] = {
  { &hf_epsli_pANI_Header_Info_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_PANI_Header_Info },
};

static int
dissect_epsli_SEQUENCE_OF_PANI_Header_Info(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_PANI_Header_Info_sequence_of, hf_index, ett_epsli_SEQUENCE_OF_PANI_Header_Info);

  return offset;
}


static const ber_sequence_t IMS_VoIP_Correlation_item_sequence[] = {
  { &hf_epsli_ims_iri       , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_IRI_Correlation },
  { &hf_epsli_ims_cc        , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_IRI_to_CC_Correlation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_IMS_VoIP_Correlation_item(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   IMS_VoIP_Correlation_item_sequence, hf_index, ett_epsli_IMS_VoIP_Correlation_item);

  return offset;
}


static const ber_sequence_t IMS_VoIP_Correlation_set_of[1] = {
  { &hf_epsli_IMS_VoIP_Correlation_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_IMS_VoIP_Correlation_item },
};

static int
dissect_epsli_IMS_VoIP_Correlation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 IMS_VoIP_Correlation_set_of, hf_index, ett_epsli_IMS_VoIP_Correlation);

  return offset;
}


static const value_string epsli_LogicalFunctionType_vals[] = {
  {   0, "pDNGW" },
  {   1, "mME" },
  {   2, "sGW" },
  {   3, "ePDG" },
  {   4, "hSS" },
  { 0, NULL }
};


static int
dissect_epsli_LogicalFunctionType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_PrintableString_SIZE_7_25(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_PrintableString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t DataNodeIdentifier_sequence[] = {
  { &hf_epsli_dataNodeAddress, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_logicalFunctionType, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_LogicalFunctionType },
  { &hf_epsli_dataNodeName  , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_7_25 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_DataNodeIdentifier(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   DataNodeIdentifier_sequence, hf_index, ett_epsli_DataNodeIdentifier);

  return offset;
}


static const ber_sequence_t Current_Previous_Systems_sequence[] = {
  { &hf_epsli_serving_System_Identifier, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_current_Serving_MME_Address, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_DataNodeIdentifier },
  { &hf_epsli_previous_Serving_System_Identifier, BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_previous_Serving_MME_Address, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_DataNodeIdentifier },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Current_Previous_Systems(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   Current_Previous_Systems_sequence, hf_index, ett_epsli_Current_Previous_Systems);

  return offset;
}


static const ber_sequence_t Change_Of_Target_Identity_sequence[] = {
  { &hf_epsli_new_MSISDN    , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_A_MSISDN  , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_MSISDN    , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_A_MSISDN  , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_IMSI      , BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_IMSI      , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_IMEI      , BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_IMEI      , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_IMPI      , BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_IMPI      , BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_SIP_URI   , BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_SIP_URI   , BER_CLASS_CON, 12, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_new_TEL_URI   , BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_old_TEL_URI   , BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_Change_Of_Target_Identity(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   Change_Of_Target_Identity_sequence, hf_index, ett_epsli_Change_Of_Target_Identity);

  return offset;
}


static const value_string epsli_Requesting_Node_Type_vals[] = {
  {   1, "mSC" },
  {   2, "sMS-Centre" },
  {   3, "gMLC" },
  {   4, "mME" },
  {   5, "sGSN" },
  { 0, NULL }
};


static int
dissect_epsli_Requesting_Node_Type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_ProSeTargetType_vals[] = {
  {   1, "pRoSeRemoteUE" },
  {   2, "pRoSeUEtoNwRelay" },
  { 0, NULL }
};


static int
dissect_epsli_ProSeTargetType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t SEQUENCE_OF_PartyInformation_sequence_of[1] = {
  { &hf_epsli_otherIdentities_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_PartyInformation },
};

static int
dissect_epsli_SEQUENCE_OF_PartyInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_PartyInformation_sequence_of, hf_index, ett_epsli_SEQUENCE_OF_PartyInformation);

  return offset;
}


static const value_string epsli_DeregistrationReason_vals[] = {
  {   1, "reason-CodeAVP" },
  {   2, "server-AssignmentType" },
  { 0, NULL }
};

static const ber_choice_t DeregistrationReason_choice[] = {
  {   1, &hf_epsli_reason_CodeAVP, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  {   2, &hf_epsli_server_AssignmentType, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_DeregistrationReason(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 DeregistrationReason_choice, hf_index, ett_epsli_DeregistrationReason,
                                 NULL);

  return offset;
}


static const value_string epsli_VoIPRoamingIndication_vals[] = {
  {   1, "roamingLBO" },
  {   2, "roamingS8HR" },
  { 0, NULL }
};


static int
dissect_epsli_VoIPRoamingIndication(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_CSREvent_vals[] = {
  {   1, "cSREventMessage" },
  { 0, NULL }
};


static int
dissect_epsli_CSREvent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t AccessPolicyType_sequence[] = {
  { &hf_epsli_userAccessPolicyAttempt, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_groupAuthorizationRulesAttempt, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_userAccessPolicyQuery, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_groupAuthorizationRulesQuery, BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_userAccessPolicyResult, BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_groupAuthorizationRulesResult, BER_CLASS_CON, 6, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_AccessPolicyType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AccessPolicyType_sequence, hf_index, ett_epsli_AccessPolicyType);

  return offset;
}


static const value_string epsli_AlertIndicator_vals[] = {
  {   1, "sent" },
  {   2, "received" },
  {   3, "cancelled" },
  { 0, NULL }
};


static int
dissect_epsli_AlertIndicator(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_PresenceType_vals[] = {
  {   1, "pTCClient" },
  {   2, "pTCGroup" },
  { 0, NULL }
};


static int
dissect_epsli_PresenceType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t AssociatePresenceStatus_sequence[] = {
  { &hf_epsli_presenceID    , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_presenceType  , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_PresenceType },
  { &hf_epsli_presenceStatus, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_AssociatePresenceStatus(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AssociatePresenceStatus_sequence, hf_index, ett_epsli_AssociatePresenceStatus);

  return offset;
}


static const value_string epsli_Emergency_vals[] = {
  {   1, "imminent" },
  {   2, "peril" },
  {   3, "cancel" },
  { 0, NULL }
};


static int
dissect_epsli_Emergency(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_T_clientEmergencyState_vals[] = {
  {   1, "inform" },
  {   2, "response" },
  {   3, "cancelInform" },
  {   4, "cancelResponse" },
  { 0, NULL }
};


static int
dissect_epsli_T_clientEmergencyState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_T_groupEmergencyState_vals[] = {
  {   1, "inForm" },
  {   2, "reSponse" },
  {   3, "cancelInform" },
  {   4, "cancelResponse" },
  { 0, NULL }
};


static int
dissect_epsli_T_groupEmergencyState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t EmergencyGroupState_sequence[] = {
  { &hf_epsli_clientEmergencyState, BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_clientEmergencyState },
  { &hf_epsli_groupEmergencyState, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_T_groupEmergencyState },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EmergencyGroupState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EmergencyGroupState_sequence, hf_index, ett_epsli_EmergencyGroupState);

  return offset;
}


static const value_string epsli_PTCType_vals[] = {
  {   1, "pTCStartofInterception" },
  {   2, "pTCServinSystem" },
  {   3, "pTCSessionInitiation" },
  {   4, "pTCSessionAbandonEndRecord" },
  {   5, "pTCSessionStartContinueRecord" },
  {   6, "pTCSessionEndRecord" },
  {   7, "pTCPre-EstablishedSessionSessionRecord" },
  {   8, "pTCInstantPersonalAlert" },
  {   9, "pTCPartyJoin" },
  {  10, "pTCPartyDrop" },
  {  11, "pTCPartyHold-RetrieveRecord" },
  {  12, "pTCMediaModification" },
  {  13, "pTCGroupAdvertizement" },
  {  14, "pTCFloorConttrol" },
  {  15, "pTCTargetPressence" },
  {  16, "pTCAssociatePressence" },
  {  17, "pTCListManagementEvents" },
  {  18, "pTCAccessPolicyEvents" },
  {  19, "pTCMediaTypeNotification" },
  {  20, "pTCGroupCallRequest" },
  {  21, "pTCGroupCallCancel" },
  {  22, "pTCGroupCallResponse" },
  {  23, "pTCGroupCallInterrogate" },
  {  24, "pTCMCPTTImminentGroupCall" },
  {  25, "pTCCC" },
  {  26, "pTCRegistration" },
  {  27, "pTCEncryption" },
  { 0, NULL }
};


static int
dissect_epsli_PTCType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t FloorActivity_sequence[] = {
  { &hf_epsli_tBCP_Request  , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Granted  , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Deny     , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Queued   , BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Release  , BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Revoke   , BER_CLASS_CON, 6, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Taken    , BER_CLASS_CON, 7, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_tBCP_Idle     , BER_CLASS_CON, 8, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_FloorActivity(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   FloorActivity_sequence, hf_index, ett_epsli_FloorActivity);

  return offset;
}



static int
dissect_epsli_VisibleString(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_VisibleString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t PTCAddress_sequence[] = {
  { &hf_epsli_uri           , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_privacy_setting, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_privacy_alias , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_VisibleString },
  { &hf_epsli_nickname      , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PTCAddress(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PTCAddress_sequence, hf_index, ett_epsli_PTCAddress);

  return offset;
}


static const value_string epsli_GroupAuthRule_vals[] = {
  {   0, "allow-Initiating-PtcSession" },
  {   1, "block-Initiating-PtcSession" },
  {   2, "allow-Joining-PtcSession" },
  {   3, "block-Joining-PtcSession" },
  {   4, "allow-Add-Participants" },
  {   5, "block-Add-Participants" },
  {   6, "allow-Subscription-PtcSession-State" },
  {   7, "block-Subscription-PtcSession-State" },
  {   8, "allow-Anonymity" },
  {   9, "forbid-Anonymity" },
  { 0, NULL }
};


static int
dissect_epsli_GroupAuthRule(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_ImminentPerilInd_vals[] = {
  {   1, "request" },
  {   2, "response" },
  {   3, "cancel" },
  { 0, NULL }
};


static int
dissect_epsli_ImminentPerilInd(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_ImplicitFloorReq_vals[] = {
  {   1, "join" },
  {   2, "rejoin" },
  {   3, "release" },
  { 0, NULL }
};


static int
dissect_epsli_ImplicitFloorReq(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_InitiationCause_vals[] = {
  {   1, "requests" },
  {   2, "received" },
  {   3, "pTCOriginatingId" },
  { 0, NULL }
};


static int
dissect_epsli_InitiationCause(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_IPADirection_vals[] = {
  {   0, "toTarget" },
  {   1, "fromTarget" },
  { 0, NULL }
};


static int
dissect_epsli_IPADirection(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_ListManagementAction_vals[] = {
  {   1, "create" },
  {   2, "modify" },
  {   3, "retrieve" },
  {   4, "delete" },
  {   5, "notify" },
  { 0, NULL }
};


static int
dissect_epsli_ListManagementAction(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_ListManagementType_vals[] = {
  {   1, "contactListManagementAttempt" },
  {   2, "groupListManagementAttempt" },
  {   3, "contactListManagementResult" },
  {   4, "groupListManagementResult" },
  {   5, "requestSuccessful" },
  { 0, NULL }
};


static int
dissect_epsli_ListManagementType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_Priority_Level_vals[] = {
  {   0, "pre-emptive" },
  {   1, "high-priority" },
  {   2, "normal-priority" },
  {   3, "listen-only" },
  { 0, NULL }
};


static int
dissect_epsli_Priority_Level(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_PreEstStatus_vals[] = {
  {   1, "established" },
  {   2, "modify" },
  {   3, "released" },
  { 0, NULL }
};


static int
dissect_epsli_PreEstStatus(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_RegistrationRequest_vals[] = {
  {   1, "register" },
  {   2, "re-register" },
  {   3, "de-register" },
  { 0, NULL }
};


static int
dissect_epsli_RegistrationRequest(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const value_string epsli_RegistrationOutcome_vals[] = {
  {   0, "success" },
  {   1, "failure" },
  { 0, NULL }
};


static int
dissect_epsli_RegistrationOutcome(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}



static int
dissect_epsli_Port_Number(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const ber_sequence_t RTPSetting_sequence[] = {
  { &hf_epsli_ip_address    , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_port_number   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_Port_Number },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_RTPSetting(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RTPSetting_sequence, hf_index, ett_epsli_RTPSetting);

  return offset;
}



static int
dissect_epsli_Talk_burst_reason_code(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_VisibleString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t T_talk_Burst_parameters_set_of[1] = {
  { &hf_epsli_talk_Burst_parameters_item, BER_CLASS_UNI, BER_UNI_TAG_VisibleString, BER_FLAGS_NOOWNTAG, dissect_epsli_VisibleString },
};

static int
dissect_epsli_T_talk_Burst_parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_set_of(implicit_tag, actx, tree, tvb, offset,
                                 T_talk_Burst_parameters_set_of, hf_index, ett_epsli_T_talk_Burst_parameters);

  return offset;
}


static const ber_sequence_t TalkburstControlSetting_sequence[] = {
  { &hf_epsli_talk_BurstControlProtocol, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_talk_Burst_parameters, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_T_talk_Burst_parameters },
  { &hf_epsli_tBCP_PortNumber, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_TalkburstControlSetting(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   TalkburstControlSetting_sequence, hf_index, ett_epsli_TalkburstControlSetting);

  return offset;
}


static const ber_sequence_t PTC_sequence[] = {
  { &hf_epsli_abandonCause  , BER_CLASS_CON, 1, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_accessPolicyFailure, BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_accessPolicyType, BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_AccessPolicyType },
  { &hf_epsli_alertIndicator, BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_AlertIndicator },
  { &hf_epsli_associatePresenceStatus, BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_AssociatePresenceStatus },
  { &hf_epsli_bearer_capability, BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_broadcastIndicator, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_contactID     , BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_emergency     , BER_CLASS_CON, 10, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Emergency },
  { &hf_epsli_emergencyGroupState, BER_CLASS_CON, 11, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EmergencyGroupState },
  { &hf_epsli_timeStamp     , BER_CLASS_CON, 12, BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TimeStamp },
  { &hf_epsli_pTCType       , BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PTCType },
  { &hf_epsli_failureCode   , BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_floorActivity , BER_CLASS_CON, 15, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_FloorActivity },
  { &hf_epsli_floorSpeakerID, BER_CLASS_CON, 16, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PTCAddress },
  { &hf_epsli_groupAdSender , BER_CLASS_CON, 17, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_groupAuthRule , BER_CLASS_CON, 19, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_GroupAuthRule },
  { &hf_epsli_groupCharacteristics, BER_CLASS_CON, 20, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_holdRetrieveInd, BER_CLASS_CON, 21, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_imminentPerilInd, BER_CLASS_CON, 23, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ImminentPerilInd },
  { &hf_epsli_implicitFloorReq, BER_CLASS_CON, 24, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ImplicitFloorReq },
  { &hf_epsli_initiationCause, BER_CLASS_CON, 25, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_InitiationCause },
  { &hf_epsli_invitationCause, BER_CLASS_CON, 26, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_iPAPartyID    , BER_CLASS_CON, 27, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_iPADirection  , BER_CLASS_CON, 28, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_IPADirection },
  { &hf_epsli_listManagementAction, BER_CLASS_CON, 29, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ListManagementAction },
  { &hf_epsli_listManagementFailure, BER_CLASS_CON, 30, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_listManagementType, BER_CLASS_CON, 31, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ListManagementType },
  { &hf_epsli_maxTBTime     , BER_CLASS_CON, 32, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_mCPTTGroupID  , BER_CLASS_CON, 33, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_mCPTTID       , BER_CLASS_CON, 34, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_mCPTTInd      , BER_CLASS_CON, 35, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_location      , BER_CLASS_CON, 36, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Location },
  { &hf_epsli_mCPTTOrganizationName, BER_CLASS_CON, 37, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_mediaStreamAvail, BER_CLASS_CON, 38, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_priority_Level, BER_CLASS_CON, 40, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Priority_Level },
  { &hf_epsli_preEstSessionID, BER_CLASS_CON, 41, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_preEstStatus  , BER_CLASS_CON, 42, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PreEstStatus },
  { &hf_epsli_pTCGroupID    , BER_CLASS_CON, 43, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCIDList     , BER_CLASS_CON, 44, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCMediaCapability, BER_CLASS_CON, 45, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCOriginatingId, BER_CLASS_CON, 46, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCOther      , BER_CLASS_CON, 47, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCParticipants, BER_CLASS_CON, 48, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCParty      , BER_CLASS_CON, 49, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCPartyDrop  , BER_CLASS_CON, 50, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCSessionInfo, BER_CLASS_CON, 51, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCServerURI  , BER_CLASS_CON, 52, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCUserAccessPolicy, BER_CLASS_CON, 53, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCAddress    , BER_CLASS_CON, 54, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PTCAddress },
  { &hf_epsli_queuedFloorControl, BER_CLASS_CON, 55, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_BOOLEAN },
  { &hf_epsli_queuedPosition, BER_CLASS_CON, 56, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_registrationRequest, BER_CLASS_CON, 57, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_RegistrationRequest },
  { &hf_epsli_registrationOutcome, BER_CLASS_CON, 58, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_RegistrationOutcome },
  { &hf_epsli_retrieveID    , BER_CLASS_CON, 59, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_rTPSetting    , BER_CLASS_CON, 60, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_RTPSetting },
  { &hf_epsli_talkBurstPriority, BER_CLASS_CON, 61, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Priority_Level },
  { &hf_epsli_talkBurstReason, BER_CLASS_CON, 62, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Talk_burst_reason_code },
  { &hf_epsli_talkburstControlSetting, BER_CLASS_CON, 63, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_TalkburstControlSetting },
  { &hf_epsli_targetPresenceStatus, BER_CLASS_CON, 64, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_port_Number   , BER_CLASS_CON, 65, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PTC(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PTC_sequence, hf_index, ett_epsli_PTC);

  return offset;
}


static const ber_sequence_t PTCEncryptionInfo_sequence[] = {
  { &hf_epsli_cipher        , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_cryptoContext , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_key           , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_keyEncoding   , BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_salt          , BER_CLASS_CON, 5, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_pTCOther      , BER_CLASS_CON, 6, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PTCEncryptionInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PTCEncryptionInfo_sequence, hf_index, ett_epsli_PTCEncryptionInfo);

  return offset;
}



static int
dissect_epsli_MCC(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_NumericString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}



static int
dissect_epsli_MNC(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_restricted_string(implicit_tag, BER_UNI_TAG_NumericString,
                                            actx, tree, tvb, offset, hf_index,
                                            NULL);

  return offset;
}


static const ber_sequence_t PLMNID_sequence[] = {
  { &hf_epsli_mCC           , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_MCC },
  { &hf_epsli_mNC           , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_MNC },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_PLMNID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PLMNID_sequence, hf_index, ett_epsli_PLMNID);

  return offset;
}



static int
dissect_epsli_NRCellID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_bitstring(implicit_tag, actx, tree, tvb, offset,
                                    NULL, 0, hf_index, -1,
                                    NULL);

  return offset;
}


static const ber_sequence_t NCGI_sequence[] = {
  { &hf_epsli_pLMNID        , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PLMNID },
  { &hf_epsli_nRCellID      , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_NRCellID },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_NCGI(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   NCGI_sequence, hf_index, ett_epsli_NCGI);

  return offset;
}


static const ber_sequence_t AdditionalCellID_sequence[] = {
  { &hf_epsli_nCGI          , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_NCGI },
  { &hf_epsli_gsmLocation   , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_GSMLocation },
  { &hf_epsli_umtsLocation  , BER_CLASS_CON, 3, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_UMTSLocation },
  { &hf_epsli_timeOfLocation, BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_GeneralizedTime },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_AdditionalCellID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AdditionalCellID_sequence, hf_index, ett_epsli_AdditionalCellID);

  return offset;
}


static const ber_sequence_t SEQUENCE_OF_AdditionalCellID_sequence_of[1] = {
  { &hf_epsli_additionalCellIDs_item, BER_CLASS_UNI, BER_UNI_TAG_SEQUENCE, BER_FLAGS_NOOWNTAG, dissect_epsli_AdditionalCellID },
};

static int
dissect_epsli_SEQUENCE_OF_AdditionalCellID(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_AdditionalCellID_sequence_of, hf_index, ett_epsli_SEQUENCE_OF_AdditionalCellID);

  return offset;
}


static const value_string epsli_ICE_type_vals[] = {
  {   1, "sgsn" },
  {   2, "ggsn" },
  {   3, "s-GW" },
  {   4, "pDN-GW" },
  {   5, "colocated-SAE-GWs" },
  {   6, "ePDG" },
  { 0, NULL }
};


static int
dissect_epsli_ICE_type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                  NULL);

  return offset;
}


static const ber_sequence_t IRI_Parameters_sequence[] = {
  { &hf_epsli_hi2epsDomainId, BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_OBJECT_IDENTIFIER },
  { &hf_epsli_lawfulInterceptionIdentifier, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_LawfulInterceptionIdentifier },
  { &hf_epsli_timeStamp     , BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TimeStamp },
  { &hf_epsli_initiator     , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_T_initiator },
  { &hf_epsli_locationOfTheTarget, BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Location },
  { &hf_epsli_partyInformation, BER_CLASS_CON, 9, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_SET_SIZE_1_10_OF_PartyInformation },
  { &hf_epsli_serviceCenterAddress, BER_CLASS_CON, 13, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PartyInformation },
  { &hf_epsli_sMS           , BER_CLASS_CON, 14, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_SMS_report },
  { &hf_epsli_national_Parameters, BER_CLASS_CON, 16, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_National_Parameters },
  { &hf_epsli_ePSCorrelationNumber, BER_CLASS_CON, 18, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPSCorrelationNumber },
  { &hf_epsli_ePSevent      , BER_CLASS_CON, 20, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPSEvent },
  { &hf_epsli_sgsnAddress   , BER_CLASS_CON, 21, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_gPRSOperationErrorCode, BER_CLASS_CON, 22, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_GPRSOperationErrorCode },
  { &hf_epsli_ggsnAddress   , BER_CLASS_CON, 24, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_qOS           , BER_CLASS_CON, 25, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_UmtsQos },
  { &hf_epsli_networkIdentifier, BER_CLASS_CON, 26, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Network_Identifier },
  { &hf_epsli_sMSOriginatingAddress, BER_CLASS_CON, 27, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_sMSTerminatingAddress, BER_CLASS_CON, 28, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DataNodeAddress },
  { &hf_epsli_iMSevent      , BER_CLASS_CON, 29, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_IMSevent },
  { &hf_epsli_sIPMessage    , BER_CLASS_CON, 30, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_servingSGSN_number, BER_CLASS_CON, 31, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_20 },
  { &hf_epsli_servingSGSN_address, BER_CLASS_CON, 32, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_5_17 },
  { &hf_epsli_ldiEvent      , BER_CLASS_CON, 34, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_LDIevent },
  { &hf_epsli_correlation   , BER_CLASS_CON, 35, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_CorrelationValues },
  { &hf_epsli_ePS_GTPV2_specificParameters, BER_CLASS_CON, 36, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPS_GTPV2_SpecificParameters },
  { &hf_epsli_ePS_PMIP_specificParameters, BER_CLASS_CON, 37, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPS_PMIP_SpecificParameters },
  { &hf_epsli_ePS_DSMIP_SpecificParameters, BER_CLASS_CON, 38, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPS_DSMIP_SpecificParameters },
  { &hf_epsli_ePS_MIP_SpecificParameters, BER_CLASS_CON, 39, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_EPS_MIP_SpecificParameters },
  { &hf_epsli_servingNodeAddress, BER_CLASS_CON, 40, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_visitedNetworkId, BER_CLASS_CON, 41, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_mediaDecryption_info, BER_CLASS_CON, 42, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_MediaDecryption_info },
  { &hf_epsli_servingS4_SGSN_address, BER_CLASS_CON, 43, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_sipMessageHeaderOffer, BER_CLASS_CON, 44, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_sipMessageHeaderAnswer, BER_CLASS_CON, 45, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_sdpOffer      , BER_CLASS_CON, 46, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_sdpAnswer     , BER_CLASS_CON, 47, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_uLITimestamp  , BER_CLASS_CON, 48, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_packetDataHeaderInformation, BER_CLASS_CON, 49, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_PacketDataHeaderInformation },
  { &hf_epsli_mediaSecFailureIndication, BER_CLASS_CON, 50, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_MediaSecFailureIndication },
  { &hf_epsli_csgIdentity   , BER_CLASS_CON, 51, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_4 },
  { &hf_epsli_heNBIdentity  , BER_CLASS_CON, 52, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_heNBiPAddress , BER_CLASS_CON, 53, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_IPAddress },
  { &hf_epsli_heNBLocation  , BER_CLASS_CON, 54, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_HeNBLocation },
  { &hf_epsli_tunnelProtocol, BER_CLASS_CON, 55, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TunnelProtocol },
  { &hf_epsli_pANI_Header_Info, BER_CLASS_CON, 56, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_SEQUENCE_OF_PANI_Header_Info },
  { &hf_epsli_imsVoIP       , BER_CLASS_CON, 57, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_IMS_VoIP_Correlation },
  { &hf_epsli_xCAPmessage   , BER_CLASS_CON, 58, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_logicalFunctionInformation, BER_CLASS_CON, 59, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_DataNodeIdentifier },
  { &hf_epsli_ccUnavailableReason, BER_CLASS_CON, 60, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString },
  { &hf_epsli_carrierSpecificData, BER_CLASS_CON, 61, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_current_previous_systems, BER_CLASS_CON, 62, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Current_Previous_Systems },
  { &hf_epsli_change_Of_Target_Identity, BER_CLASS_CON, 63, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Change_Of_Target_Identity },
  { &hf_epsli_requesting_Network_Identifier, BER_CLASS_CON, 64, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_requesting_Node_Type, BER_CLASS_CON, 65, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_Requesting_Node_Type },
  { &hf_epsli_serving_System_Identifier, BER_CLASS_CON, 66, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_proSeTargetType, BER_CLASS_CON, 67, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ProSeTargetType },
  { &hf_epsli_proSeRelayMSISDN, BER_CLASS_CON, 68, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_1_9 },
  { &hf_epsli_proSeRelayIMSI, BER_CLASS_CON, 69, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_3_8 },
  { &hf_epsli_proSeRelayIMEI, BER_CLASS_CON, 70, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING_SIZE_8 },
  { &hf_epsli_extendedLocParameters, BER_CLASS_CON, 71, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ExtendedLocParameters },
  { &hf_epsli_locationErrorCode, BER_CLASS_CON, 72, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_LocationErrorCode },
  { &hf_epsli_otherIdentities, BER_CLASS_CON, 73, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_SEQUENCE_OF_PartyInformation },
  { &hf_epsli_deregistrationReason, BER_CLASS_CON, 74, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_DeregistrationReason },
  { &hf_epsli_requesting_Node_Identifier, BER_CLASS_CON, 75, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_OCTET_STRING },
  { &hf_epsli_roamingIndication, BER_CLASS_CON, 76, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_VoIPRoamingIndication },
  { &hf_epsli_cSREvent      , BER_CLASS_CON, 77, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_CSREvent },
  { &hf_epsli_ptc           , BER_CLASS_CON, 78, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PTC },
  { &hf_epsli_ptcEncryption , BER_CLASS_CON, 79, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_PTCEncryptionInfo },
  { &hf_epsli_additionalCellIDs, BER_CLASS_CON, 80, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_SEQUENCE_OF_AdditionalCellID },
  { &hf_epsli_scefID        , BER_CLASS_CON, 81, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_UTF8String },
  { &hf_epsli_ice_type      , BER_CLASS_CON, 254, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ICE_type },
  { &hf_epsli_national_HI2_ASN1parameters, BER_CLASS_CON, 255, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_National_HI2_ASN1parameters },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_IRI_Parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   IRI_Parameters_sequence, hf_index, ett_epsli_IRI_Parameters);

  return offset;
}


static const value_string epsli_EpsIRIContent_vals[] = {
  {   0, "keep-Alive" },
  {   1, "iRI-Begin-record" },
  {   2, "iRI-End-record" },
  {   3, "iRI-Continue-record" },
  {   4, "iRI-Report-record" },
  { 0, NULL }
};

static const ber_choice_t EpsIRIContent_choice[] = {
  {   0, &hf_epsli_keep_Alive    , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_NULL },
  {   1, &hf_epsli_iRI_Begin_record, BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_Parameters },
  {   2, &hf_epsli_iRI_End_record, BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_Parameters },
  {   3, &hf_epsli_iRI_Continue_record, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_Parameters },
  {   4, &hf_epsli_iRI_Report_record, BER_CLASS_CON, 4, BER_FLAGS_IMPLTAG, dissect_epsli_IRI_Parameters },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_EpsIRIContent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_choice(actx, tree, tvb, offset,
                                 EpsIRIContent_choice, hf_index, ett_epsli_EpsIRIContent,
                                 NULL);

  return offset;
}


static const ber_sequence_t National_HI3_ASN1parameters_sequence[] = {
  { &hf_epsli_countryCode   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_PrintableString_SIZE_2 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_National_HI3_ASN1parameters(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   National_HI3_ASN1parameters_sequence, hf_index, ett_epsli_National_HI3_ASN1parameters);

  return offset;
}


static const ber_sequence_t ULIC_header_sequence[] = {
  { &hf_epsli_hi3DomainId   , BER_CLASS_CON, 0, BER_FLAGS_IMPLTAG, dissect_epsli_OBJECT_IDENTIFIER },
  { &hf_epsli_lIID          , BER_CLASS_CON, 2, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_LawfulInterceptionIdentifier },
  { &hf_epsli_correlation_Number, BER_CLASS_CON, 3, BER_FLAGS_IMPLTAG, dissect_epsli_EPSCorrelationNumber },
  { &hf_epsli_timeStamp     , BER_CLASS_CON, 4, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG|BER_FLAGS_NOTCHKTAG, dissect_epsli_TimeStamp },
  { &hf_epsli_sequence_number, BER_CLASS_CON, 5, BER_FLAGS_IMPLTAG, dissect_epsli_INTEGER_0_65535 },
  { &hf_epsli_t_PDU_direction, BER_CLASS_CON, 6, BER_FLAGS_IMPLTAG, dissect_epsli_TPDU_direction },
  { &hf_epsli_national_HI3_ASN1parameters, BER_CLASS_CON, 7, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_National_HI3_ASN1parameters },
  { &hf_epsli_ice_type      , BER_CLASS_CON, 8, BER_FLAGS_OPTIONAL|BER_FLAGS_IMPLTAG, dissect_epsli_ICE_type },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_ULIC_header(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ULIC_header_sequence, hf_index, ett_epsli_ULIC_header);

  return offset;
}



static int
dissect_epsli_T_payload(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 25 "./asn1/epsli/epsli.cnf"
  int start=offset;

  port_type type;
  address src_addr;
	address dst_addr;
	guint32 src_port;
	guint32 dst_port;
  proto_tree *parent=NULL;
  packet_info *pinfo = actx->pinfo;

    offset = dissect_ber_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);


  if (tree && tree->parent){
    parent=tree->parent;
    tree->parent=NULL;
  }
  /* to make TCP reassemle success*/
  type=pinfo->ptype;
  copy_address_shallow(&src_addr, &pinfo->src);
	copy_address_shallow(&dst_addr, &pinfo->dst);
	src_port = pinfo->srcport;
	dst_port = pinfo->destport;
  
  if (tvb_get_guint8(tvb, start)==0x45)
    call_dissector(ip_handle, tvb, pinfo, tree);
  else if ((tvb_get_guint8(tvb, start) & 0xf0)==0x60)
    call_dissector(ipv6_handle, tvb, pinfo, tree);
  
  pinfo->ptype=type;
  copy_address_shallow(&pinfo->src, &src_addr);
	copy_address_shallow(&pinfo->dst, &dst_addr);
	pinfo->srcport = src_port;
	pinfo->destport = dst_port;
  if (parent)
      tree->parent=parent;


  return offset;
}


static const ber_sequence_t CC_PDU_sequence[] = {
  { &hf_epsli_uLIC_header   , BER_CLASS_CON, 1, BER_FLAGS_IMPLTAG, dissect_epsli_ULIC_header },
  { &hf_epsli_payload       , BER_CLASS_CON, 2, BER_FLAGS_IMPLTAG, dissect_epsli_T_payload },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_epsli_CC_PDU(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_ber_sequence(implicit_tag, actx, tree, tvb, offset,
                                   CC_PDU_sequence, hf_index, ett_epsli_CC_PDU);

  return offset;
}

/*--- PDUs ---*/

static int dissect_EpsIRIContent_PDU(tvbuff_t *tvb _U_, packet_info *pinfo _U_, proto_tree *tree _U_, void *data _U_) {
  int offset = 0;
  asn1_ctx_t asn1_ctx;
  asn1_ctx_init(&asn1_ctx, ASN1_ENC_BER, TRUE, pinfo);
  offset = dissect_epsli_EpsIRIContent(FALSE, tvb, offset, &asn1_ctx, tree, hf_epsli_EpsIRIContent_PDU);
  return offset;
}
static int dissect_CC_PDU_PDU(tvbuff_t *tvb _U_, packet_info *pinfo _U_, proto_tree *tree _U_, void *data _U_) {
  int offset = 0;
  asn1_ctx_t asn1_ctx;
  asn1_ctx_init(&asn1_ctx, ASN1_ENC_BER, TRUE, pinfo);
  offset = dissect_epsli_CC_PDU(FALSE, tvb, offset, &asn1_ctx, tree, hf_epsli_CC_PDU_PDU);
  return offset;
}


/*--- End of included file: packet-epsli-fn.c ---*/
#line 43 "./asn1/epsli/packet-epsli-template.c"


static gboolean
dissect_x2IRI_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
  proto_item *iri_item = NULL;
  proto_tree *iri_tree = NULL;
  tvbuff_t   *next_tvb;
  guint8 firstByte;
  guint32 len, totalLen, offset=0; 

  totalLen=tvb_reported_length(tvb);
  do{
    /* the total len should at least xx bytes */
    //if (totalLen<20)
      //return FALSE;

    /* the TPKT header */
    firstByte=tvb_get_guint8(tvb, offset);
    if (firstByte != 3)
      return FALSE;

    if (tvb_get_guint8(tvb, offset+1)!=0)
      return FALSE;
    
    len=tvb_get_ntohs(tvb, offset+2);
    if(len>totalLen-offset)
      return FALSE;

    iri_item = proto_tree_add_item(tree, proto_epsli, tvb, offset, offset+len, ENC_NA);
    iri_tree = proto_item_add_subtree(iri_item, ett_iri);
    offset += 4;

    next_tvb = tvb_new_subset_length(tvb, offset, tvb_reported_length_remaining(tvb, offset));
    offset+=dissect_EpsIRIContent_PDU(next_tvb, pinfo, iri_tree, data);

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "EPSLI");
    col_clear_fence(pinfo->cinfo, COL_INFO);
    col_clear(pinfo->cinfo, COL_INFO);
    col_set_str(pinfo->cinfo, COL_INFO, "x2 PDU");
  }while(offset<totalLen); 
  
  return TRUE;
}

static gboolean
dissect_x3cc_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
  proto_item *iri_item = NULL;
  proto_tree *iri_tree = NULL;
  tvbuff_t   *next_tvb;
  guint8 firstByte, secondByte;
  guint32 len, totalLen, offset=0; 

  totalLen=tvb_reported_length(tvb);
  do{
    firstByte=tvb_get_guint8(tvb, offset);
    if (firstByte != 0x30)
      return FALSE;
    
    secondByte=tvb_get_guint8(tvb, offset+1);
    if (secondByte != 0x82)
      return FALSE;
    
    len=tvb_get_ntohs(tvb, offset+2);
    if(len > totalLen-offset-4){
      if (pinfo->ptype==PT_TCP){
        /* TCP reassemble */
        pinfo->desegment_offset = offset;
        pinfo->desegment_len = DESEGMENT_ONE_MORE_SEGMENT;
        break;
      }
      else
        return FALSE;
    }   

    iri_item = proto_tree_add_item(tree, proto_epsli, tvb, offset, offset+len+4, ENC_NA);
    iri_tree = proto_item_add_subtree(iri_item, ett_iri);
    next_tvb = tvb_new_subset_length(tvb, offset, tvb_reported_length_remaining(tvb, offset));
    offset+=dissect_CC_PDU_PDU(next_tvb, pinfo, iri_tree, data);
  }while(offset<totalLen);    
  
  col_set_str(pinfo->cinfo, COL_PROTOCOL, "EPSLI");
  col_clear_fence(pinfo->cinfo, COL_INFO);
  col_clear(pinfo->cinfo, COL_INFO);
  col_set_str(pinfo->cinfo, COL_INFO, "x3 PDU");

  return TRUE;
}

/*--- proto_register_epsli -------------------------------------------*/
void proto_register_epsli(void) {

  /* List of fields */
  static hf_register_info hf[] = {

/*--- Included file: packet-epsli-hfarr.c ---*/
#line 1 "./asn1/epsli/packet-epsli-hfarr.c"
    { &hf_epsli_EpsIRIContent_PDU,
      { "EpsIRIContent", "epsli.EpsIRIContent",
        FT_UINT32, BASE_DEC, VALS(epsli_EpsIRIContent_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_CC_PDU_PDU,
      { "CC-PDU", "epsli.CC_PDU_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_localTime,
      { "localTime", "epsli.localTime_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "LocalTimeStamp", HFILL }},
    { &hf_epsli_utcTime,
      { "utcTime", "epsli.utcTime",
        FT_STRING, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_generalizedTime,
      { "generalizedTime", "epsli.generalizedTime",
        FT_STRING, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_winterSummerIndication,
      { "winterSummerIndication", "epsli.winterSummerIndication",
        FT_UINT32, BASE_DEC, VALS(epsli_T_winterSummerIndication_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_operator_Identifier,
      { "operator-Identifier", "epsli.operator_Identifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_5", HFILL }},
    { &hf_epsli_network_Element_Identifier,
      { "network-Element-Identifier", "epsli.network_Element_Identifier",
        FT_UINT32, BASE_DEC, VALS(epsli_Network_Element_Identifier_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_e164_Format,
      { "e164-Format", "epsli.e164_Format",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_25", HFILL }},
    { &hf_epsli_x25_Format,
      { "x25-Format", "epsli.x25_Format",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_25", HFILL }},
    { &hf_epsli_iP_Format,
      { "iP-Format", "epsli.iP_Format",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_25", HFILL }},
    { &hf_epsli_dNS_Format,
      { "dNS-Format", "epsli.dNS_Format",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_25", HFILL }},
    { &hf_epsli_iP_Address,
      { "iP-Address", "epsli.iP_Address_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IPAddress", HFILL }},
    { &hf_epsli_National_Parameters_item,
      { "National-Parameters item", "epsli.National_Parameters_item",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_256", HFILL }},
    { &hf_epsli_countryCode,
      { "countryCode", "epsli.countryCode",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_2", HFILL }},
    { &hf_epsli_ipAddress,
      { "ipAddress", "epsli.ipAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_x25Address,
      { "x25Address", "epsli.x25Address",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_iP_type,
      { "iP-type", "epsli.iP_type",
        FT_UINT32, BASE_DEC, VALS(epsli_T_iP_type_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_iP_value,
      { "iP-value", "epsli.iP_value",
        FT_UINT32, BASE_DEC, VALS(epsli_IP_value_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_iP_assignment,
      { "iP-assignment", "epsli.iP_assignment",
        FT_UINT32, BASE_DEC, VALS(epsli_T_iP_assignment_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_iPBinaryAddress,
      { "iPBinaryAddress", "epsli.iPBinaryAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4_16", HFILL }},
    { &hf_epsli_iPTextAddress,
      { "iPTextAddress", "epsli.iPTextAddress",
        FT_STRING, BASE_NONE, NULL, 0,
        "IA5String_SIZE_7_45", HFILL }},
    { &hf_epsli_detailedCivicAddress,
      { "detailedCivicAddress", "epsli.detailedCivicAddress",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SET_OF_DetailedCivicAddress", HFILL }},
    { &hf_epsli_detailedCivicAddress_item,
      { "DetailedCivicAddress", "epsli.DetailedCivicAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_xmlCivicAddress,
      { "xmlCivicAddress", "epsli.xmlCivicAddress",
        FT_STRING, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_building,
      { "building", "epsli.building",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_room,
      { "room", "epsli.room",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_placeType,
      { "placeType", "epsli.placeType",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_postalCommunityName,
      { "postalCommunityName", "epsli.postalCommunityName",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_additionalCode,
      { "additionalCode", "epsli.additionalCode",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_seat,
      { "seat", "epsli.seat",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_primaryRoad,
      { "primaryRoad", "epsli.primaryRoad",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_primaryRoadDirection,
      { "primaryRoadDirection", "epsli.primaryRoadDirection",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_trailingStreetSuffix,
      { "trailingStreetSuffix", "epsli.trailingStreetSuffix",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_streetSuffix,
      { "streetSuffix", "epsli.streetSuffix",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_houseNumber,
      { "houseNumber", "epsli.houseNumber",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_houseNumberSuffix,
      { "houseNumberSuffix", "epsli.houseNumberSuffix",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_landmarkAddress,
      { "landmarkAddress", "epsli.landmarkAddress",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_additionalLocation,
      { "additionalLocation", "epsli.additionalLocation",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_name,
      { "name", "epsli.name",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_floor,
      { "floor", "epsli.floor",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_primaryStreet,
      { "primaryStreet", "epsli.primaryStreet",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_primaryStreetDirection,
      { "primaryStreetDirection", "epsli.primaryStreetDirection",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_roadSection,
      { "roadSection", "epsli.roadSection",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_roadBranch,
      { "roadBranch", "epsli.roadBranch",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_roadSubBranch,
      { "roadSubBranch", "epsli.roadSubBranch",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_roadPreModifier,
      { "roadPreModifier", "epsli.roadPreModifier",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_roadPostModifier,
      { "roadPostModifier", "epsli.roadPostModifier",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_postalCode,
      { "postalCode", "epsli.postalCode",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_town,
      { "town", "epsli.town",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_county,
      { "county", "epsli.county",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_country,
      { "country", "epsli.country",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_language,
      { "language", "epsli.language",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_posMethod,
      { "posMethod", "epsli.posMethod",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_mapData,
      { "mapData", "epsli.mapData",
        FT_UINT32, BASE_DEC, VALS(epsli_T_mapData_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_base64Map,
      { "base64Map", "epsli.base64Map",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_url,
      { "url", "epsli.url",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_altitude,
      { "altitude", "epsli.altitude_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_alt,
      { "alt", "epsli.alt",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_alt_uncertainty,
      { "alt-uncertainty", "epsli.alt_uncertainty",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_speed,
      { "speed", "epsli.speed",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_direction,
      { "direction", "epsli.direction",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_level_conf,
      { "level-conf", "epsli.level_conf",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_qOS_not_met,
      { "qOS-not-met", "epsli.qOS_not_met",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_motionStateList,
      { "motionStateList", "epsli.motionStateList_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_primaryMotionState,
      { "primaryMotionState", "epsli.primaryMotionState",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_secondaryMotionState,
      { "secondaryMotionState", "epsli.secondaryMotionState",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_secondaryMotionState_item,
      { "secondaryMotionState item", "epsli.secondaryMotionState_item",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_confidence,
      { "confidence", "epsli.confidence",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_floor_01,
      { "floor", "epsli.floor_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_floor_number,
      { "floor-number", "epsli.floor_number",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_floor_number_uncertainty,
      { "floor-number-uncertainty", "epsli.floor_number_uncertainty",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_additional_info,
      { "additional-info", "epsli.additional_info",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_lALS_rawMLPPosData,
      { "lALS-rawMLPPosData", "epsli.lALS_rawMLPPosData",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_keep_Alive,
      { "keep-Alive", "epsli.keep_Alive_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_iRI_Begin_record,
      { "iRI-Begin-record", "epsli.iRI_Begin_record_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_Parameters", HFILL }},
    { &hf_epsli_iRI_End_record,
      { "iRI-End-record", "epsli.iRI_End_record_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_Parameters", HFILL }},
    { &hf_epsli_iRI_Continue_record,
      { "iRI-Continue-record", "epsli.iRI_Continue_record_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_Parameters", HFILL }},
    { &hf_epsli_iRI_Report_record,
      { "iRI-Report-record", "epsli.iRI_Report_record_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_Parameters", HFILL }},
    { &hf_epsli_hi2epsDomainId,
      { "hi2epsDomainId", "epsli.hi2epsDomainId",
        FT_OID, BASE_NONE, NULL, 0,
        "OBJECT_IDENTIFIER", HFILL }},
    { &hf_epsli_lawfulInterceptionIdentifier,
      { "lawfulInterceptionIdentifier", "epsli.lawfulInterceptionIdentifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_timeStamp,
      { "timeStamp", "epsli.timeStamp",
        FT_UINT32, BASE_DEC, VALS(epsli_TimeStamp_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_initiator,
      { "initiator", "epsli.initiator",
        FT_UINT32, BASE_DEC, VALS(epsli_T_initiator_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_locationOfTheTarget,
      { "locationOfTheTarget", "epsli.locationOfTheTarget_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "Location", HFILL }},
    { &hf_epsli_partyInformation,
      { "partyInformation", "epsli.partyInformation",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SET_SIZE_1_10_OF_PartyInformation", HFILL }},
    { &hf_epsli_partyInformation_item,
      { "PartyInformation", "epsli.PartyInformation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_serviceCenterAddress,
      { "serviceCenterAddress", "epsli.serviceCenterAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_sMS,
      { "sMS", "epsli.sMS_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SMS_report", HFILL }},
    { &hf_epsli_national_Parameters,
      { "national-Parameters", "epsli.national_Parameters",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePSCorrelationNumber,
      { "ePSCorrelationNumber", "epsli.ePSCorrelationNumber",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePSevent,
      { "ePSevent", "epsli.ePSevent",
        FT_UINT32, BASE_DEC, VALS(epsli_EPSEvent_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_sgsnAddress,
      { "sgsnAddress", "epsli.sgsnAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_gPRSOperationErrorCode,
      { "gPRSOperationErrorCode", "epsli.gPRSOperationErrorCode",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ggsnAddress,
      { "ggsnAddress", "epsli.ggsnAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_qOS,
      { "qOS", "epsli.qOS",
        FT_UINT32, BASE_DEC, VALS(epsli_UmtsQos_vals), 0,
        "UmtsQos", HFILL }},
    { &hf_epsli_networkIdentifier,
      { "networkIdentifier", "epsli.networkIdentifier_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "Network_Identifier", HFILL }},
    { &hf_epsli_sMSOriginatingAddress,
      { "sMSOriginatingAddress", "epsli.sMSOriginatingAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_sMSTerminatingAddress,
      { "sMSTerminatingAddress", "epsli.sMSTerminatingAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_iMSevent,
      { "iMSevent", "epsli.iMSevent",
        FT_UINT32, BASE_DEC, VALS(epsli_IMSevent_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_sIPMessage,
      { "sIPMessage", "epsli.sIPMessage",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_servingSGSN_number,
      { "servingSGSN-number", "epsli.servingSGSN_number",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_20", HFILL }},
    { &hf_epsli_servingSGSN_address,
      { "servingSGSN-address", "epsli.servingSGSN_address",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_5_17", HFILL }},
    { &hf_epsli_ldiEvent,
      { "ldiEvent", "epsli.ldiEvent",
        FT_UINT32, BASE_DEC, VALS(epsli_LDIevent_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_correlation,
      { "correlation", "epsli.correlation",
        FT_UINT32, BASE_DEC, VALS(epsli_CorrelationValues_vals), 0,
        "CorrelationValues", HFILL }},
    { &hf_epsli_ePS_GTPV2_specificParameters,
      { "ePS-GTPV2-specificParameters", "epsli.ePS_GTPV2_specificParameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePS_PMIP_specificParameters,
      { "ePS-PMIP-specificParameters", "epsli.ePS_PMIP_specificParameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePS_DSMIP_SpecificParameters,
      { "ePS-DSMIP-SpecificParameters", "epsli.ePS_DSMIP_SpecificParameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePS_MIP_SpecificParameters,
      { "ePS-MIP-SpecificParameters", "epsli.ePS_MIP_SpecificParameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_servingNodeAddress,
      { "servingNodeAddress", "epsli.servingNodeAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_visitedNetworkId,
      { "visitedNetworkId", "epsli.visitedNetworkId",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_mediaDecryption_info,
      { "mediaDecryption-info", "epsli.mediaDecryption_info",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_servingS4_SGSN_address,
      { "servingS4-SGSN-address", "epsli.servingS4_SGSN_address",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_sipMessageHeaderOffer,
      { "sipMessageHeaderOffer", "epsli.sipMessageHeaderOffer",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_sipMessageHeaderAnswer,
      { "sipMessageHeaderAnswer", "epsli.sipMessageHeaderAnswer",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_sdpOffer,
      { "sdpOffer", "epsli.sdpOffer",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_sdpAnswer,
      { "sdpAnswer", "epsli.sdpAnswer",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_uLITimestamp,
      { "uLITimestamp", "epsli.uLITimestamp",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_8", HFILL }},
    { &hf_epsli_packetDataHeaderInformation,
      { "packetDataHeaderInformation", "epsli.packetDataHeaderInformation",
        FT_UINT32, BASE_DEC, VALS(epsli_PacketDataHeaderInformation_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_mediaSecFailureIndication,
      { "mediaSecFailureIndication", "epsli.mediaSecFailureIndication",
        FT_UINT32, BASE_DEC, VALS(epsli_MediaSecFailureIndication_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_csgIdentity,
      { "csgIdentity", "epsli.csgIdentity",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_heNBIdentity,
      { "heNBIdentity", "epsli.heNBIdentity",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_heNBiPAddress,
      { "heNBiPAddress", "epsli.heNBiPAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IPAddress", HFILL }},
    { &hf_epsli_heNBLocation,
      { "heNBLocation", "epsli.heNBLocation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_tunnelProtocol,
      { "tunnelProtocol", "epsli.tunnelProtocol",
        FT_UINT32, BASE_DEC, VALS(epsli_TunnelProtocol_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_pANI_Header_Info,
      { "pANI-Header-Info", "epsli.pANI_Header_Info",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_PANI_Header_Info", HFILL }},
    { &hf_epsli_pANI_Header_Info_item,
      { "PANI-Header-Info", "epsli.PANI_Header_Info_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_imsVoIP,
      { "imsVoIP", "epsli.imsVoIP",
        FT_UINT32, BASE_DEC, NULL, 0,
        "IMS_VoIP_Correlation", HFILL }},
    { &hf_epsli_xCAPmessage,
      { "xCAPmessage", "epsli.xCAPmessage",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_logicalFunctionInformation,
      { "logicalFunctionInformation", "epsli.logicalFunctionInformation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "DataNodeIdentifier", HFILL }},
    { &hf_epsli_ccUnavailableReason,
      { "ccUnavailableReason", "epsli.ccUnavailableReason",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString", HFILL }},
    { &hf_epsli_carrierSpecificData,
      { "carrierSpecificData", "epsli.carrierSpecificData",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_current_previous_systems,
      { "current-previous-systems", "epsli.current_previous_systems_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_change_Of_Target_Identity,
      { "change-Of-Target-Identity", "epsli.change_Of_Target_Identity_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_requesting_Network_Identifier,
      { "requesting-Network-Identifier", "epsli.requesting_Network_Identifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_requesting_Node_Type,
      { "requesting-Node-Type", "epsli.requesting_Node_Type",
        FT_UINT32, BASE_DEC, VALS(epsli_Requesting_Node_Type_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_serving_System_Identifier,
      { "serving-System-Identifier", "epsli.serving_System_Identifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_proSeTargetType,
      { "proSeTargetType", "epsli.proSeTargetType",
        FT_UINT32, BASE_DEC, VALS(epsli_ProSeTargetType_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_proSeRelayMSISDN,
      { "proSeRelayMSISDN", "epsli.proSeRelayMSISDN",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_9", HFILL }},
    { &hf_epsli_proSeRelayIMSI,
      { "proSeRelayIMSI", "epsli.proSeRelayIMSI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_3_8", HFILL }},
    { &hf_epsli_proSeRelayIMEI,
      { "proSeRelayIMEI", "epsli.proSeRelayIMEI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_8", HFILL }},
    { &hf_epsli_extendedLocParameters,
      { "extendedLocParameters", "epsli.extendedLocParameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_locationErrorCode,
      { "locationErrorCode", "epsli.locationErrorCode",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_otherIdentities,
      { "otherIdentities", "epsli.otherIdentities",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_PartyInformation", HFILL }},
    { &hf_epsli_otherIdentities_item,
      { "PartyInformation", "epsli.PartyInformation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_deregistrationReason,
      { "deregistrationReason", "epsli.deregistrationReason",
        FT_UINT32, BASE_DEC, VALS(epsli_DeregistrationReason_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_requesting_Node_Identifier,
      { "requesting-Node-Identifier", "epsli.requesting_Node_Identifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_roamingIndication,
      { "roamingIndication", "epsli.roamingIndication",
        FT_UINT32, BASE_DEC, VALS(epsli_VoIPRoamingIndication_vals), 0,
        "VoIPRoamingIndication", HFILL }},
    { &hf_epsli_cSREvent,
      { "cSREvent", "epsli.cSREvent",
        FT_UINT32, BASE_DEC, VALS(epsli_CSREvent_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_ptc,
      { "ptc", "epsli.ptc_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ptcEncryption,
      { "ptcEncryption", "epsli.ptcEncryption_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PTCEncryptionInfo", HFILL }},
    { &hf_epsli_additionalCellIDs,
      { "additionalCellIDs", "epsli.additionalCellIDs",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_AdditionalCellID", HFILL }},
    { &hf_epsli_additionalCellIDs_item,
      { "AdditionalCellID", "epsli.AdditionalCellID_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_scefID,
      { "scefID", "epsli.scefID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_ice_type,
      { "ice-type", "epsli.ice_type",
        FT_UINT32, BASE_DEC, VALS(epsli_ICE_type_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_national_HI2_ASN1parameters,
      { "national-HI2-ASN1parameters", "epsli.national_HI2_ASN1parameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_dataNodeAddress,
      { "dataNodeAddress", "epsli.dataNodeAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_logicalFunctionType,
      { "logicalFunctionType", "epsli.logicalFunctionType",
        FT_UINT32, BASE_DEC, VALS(epsli_LogicalFunctionType_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_dataNodeName,
      { "dataNodeName", "epsli.dataNodeName",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_7_25", HFILL }},
    { &hf_epsli_access_Type,
      { "access-Type", "epsli.access_Type",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_access_Class,
      { "access-Class", "epsli.access_Class",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_network_Provided,
      { "network-Provided", "epsli.network_Provided_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_pANI_Location,
      { "pANI-Location", "epsli.pANI_Location_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_raw_Location,
      { "raw-Location", "epsli.raw_Location",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_location,
      { "location", "epsli.location_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ePSLocation,
      { "ePSLocation", "epsli.ePSLocation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_party_Qualifier,
      { "party-Qualifier", "epsli.party_Qualifier",
        FT_UINT32, BASE_DEC, VALS(epsli_T_party_Qualifier_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_partyIdentity,
      { "partyIdentity", "epsli.partyIdentity_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_imei,
      { "imei", "epsli.imei",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_8", HFILL }},
    { &hf_epsli_imsi,
      { "imsi", "epsli.imsi",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_3_8", HFILL }},
    { &hf_epsli_msISDN,
      { "msISDN", "epsli.msISDN",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_9", HFILL }},
    { &hf_epsli_sip_uri,
      { "sip-uri", "epsli.sip_uri",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_tel_uri,
      { "tel-uri", "epsli.tel_uri",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_nai,
      { "nai", "epsli.nai",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_x_3GPP_Asserted_Identity,
      { "x-3GPP-Asserted-Identity", "epsli.x_3GPP_Asserted_Identity",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_xUI,
      { "xUI", "epsli.xUI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_iMPI,
      { "iMPI", "epsli.iMPI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_extID,
      { "extID", "epsli.extID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_services_Data_Information,
      { "services-Data-Information", "epsli.services_Data_Information_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_e164_Number,
      { "e164-Number", "epsli.e164_Number",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_25", HFILL }},
    { &hf_epsli_globalCellID,
      { "globalCellID", "epsli.globalCellID",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_rAI,
      { "rAI", "epsli.rAI",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_gsmLocation,
      { "gsmLocation", "epsli.gsmLocation",
        FT_UINT32, BASE_DEC, VALS(epsli_GSMLocation_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_umtsLocation,
      { "umtsLocation", "epsli.umtsLocation",
        FT_UINT32, BASE_DEC, VALS(epsli_UMTSLocation_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_sAI,
      { "sAI", "epsli.sAI",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_oldRAI,
      { "oldRAI", "epsli.oldRAI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "Rai", HFILL }},
    { &hf_epsli_civicAddress,
      { "civicAddress", "epsli.civicAddress",
        FT_UINT32, BASE_DEC, VALS(epsli_CivicAddress_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_operatorSpecificInfo,
      { "operatorSpecificInfo", "epsli.operatorSpecificInfo",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_uELocationTimestamp,
      { "uELocationTimestamp", "epsli.uELocationTimestamp",
        FT_UINT32, BASE_DEC, VALS(epsli_T_uELocationTimestamp_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_timestamp,
      { "timestamp", "epsli.timestamp",
        FT_UINT32, BASE_DEC, VALS(epsli_TimeStamp_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_timestampUnknown,
      { "timestampUnknown", "epsli.timestampUnknown_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_nCGI,
      { "nCGI", "epsli.nCGI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_timeOfLocation,
      { "timeOfLocation", "epsli.timeOfLocation",
        FT_STRING, BASE_NONE, NULL, 0,
        "GeneralizedTime", HFILL }},
    { &hf_epsli_mCC,
      { "mCC", "epsli.mCC",
        FT_STRING, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_mNC,
      { "mNC", "epsli.mNC",
        FT_STRING, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_pLMNID,
      { "pLMNID", "epsli.pLMNID_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_nRCellID,
      { "nRCellID", "epsli.nRCellID",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_geoCoordinates,
      { "geoCoordinates", "epsli.geoCoordinates_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_latitude,
      { "latitude", "epsli.latitude",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_7_10", HFILL }},
    { &hf_epsli_longitude,
      { "longitude", "epsli.longitude",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_8_11", HFILL }},
    { &hf_epsli_mapDatum,
      { "mapDatum", "epsli.mapDatum",
        FT_UINT32, BASE_DEC, VALS(epsli_MapDatum_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_azimuth,
      { "azimuth", "epsli.azimuth",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_359", HFILL }},
    { &hf_epsli_utmCoordinates,
      { "utmCoordinates", "epsli.utmCoordinates_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_utm_East,
      { "utm-East", "epsli.utm_East",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_10", HFILL }},
    { &hf_epsli_utm_North,
      { "utm-North", "epsli.utm_North",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_7", HFILL }},
    { &hf_epsli_utmRefCoordinates,
      { "utmRefCoordinates", "epsli.utmRefCoordinates_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_utmref_string,
      { "utmref-string", "epsli.utmref_string",
        FT_STRING, BASE_NONE, NULL, 0,
        "PrintableString_SIZE_13", HFILL }},
    { &hf_epsli_wGS84Coordinates,
      { "wGS84Coordinates", "epsli.wGS84Coordinates",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_point,
      { "point", "epsli.point_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "GA_Point", HFILL }},
    { &hf_epsli_pointWithUnCertainty,
      { "pointWithUnCertainty", "epsli.pointWithUnCertainty_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "GA_PointWithUnCertainty", HFILL }},
    { &hf_epsli_polygon,
      { "polygon", "epsli.polygon",
        FT_UINT32, BASE_DEC, NULL, 0,
        "GA_Polygon", HFILL }},
    { &hf_epsli_latitudeSign,
      { "latitudeSign", "epsli.latitudeSign",
        FT_UINT32, BASE_DEC, VALS(epsli_T_latitudeSign_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_latitude_01,
      { "latitude", "epsli.latitude",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_8388607", HFILL }},
    { &hf_epsli_longitude_01,
      { "longitude", "epsli.longitude",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER_M8388608_8388607", HFILL }},
    { &hf_epsli_geographicalCoordinates,
      { "geographicalCoordinates", "epsli.geographicalCoordinates_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_uncertaintyCode,
      { "uncertaintyCode", "epsli.uncertaintyCode",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_127", HFILL }},
    { &hf_epsli_GA_Polygon_item,
      { "GA-Polygon item", "epsli.GA_Polygon_item_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_sMS_Contents,
      { "sMS-Contents", "epsli.sMS_Contents_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_sms_initiator,
      { "sms-initiator", "epsli.sms_initiator",
        FT_UINT32, BASE_DEC, VALS(epsli_T_sms_initiator_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_transfer_status,
      { "transfer-status", "epsli.transfer_status",
        FT_UINT32, BASE_DEC, VALS(epsli_T_transfer_status_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_other_message,
      { "other-message", "epsli.other_message",
        FT_UINT32, BASE_DEC, VALS(epsli_T_other_message_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_content,
      { "content", "epsli.content",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_270", HFILL }},
    { &hf_epsli_iri_to_CC,
      { "iri-to-CC", "epsli.iri_to_CC_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_to_CC_Correlation", HFILL }},
    { &hf_epsli_iri_to_iri,
      { "iri-to-iri", "epsli.iri_to_iri",
        FT_BYTES, BASE_NONE, NULL, 0,
        "IRI_to_IRI_Correlation", HFILL }},
    { &hf_epsli_both_IRI_CC,
      { "both-IRI-CC", "epsli.both_IRI_CC_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_iri_CC,
      { "iri-CC", "epsli.iri_CC_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_to_CC_Correlation", HFILL }},
    { &hf_epsli_iri_IRI,
      { "iri-IRI", "epsli.iri_IRI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "IRI_to_IRI_Correlation", HFILL }},
    { &hf_epsli_IMS_VoIP_Correlation_item,
      { "IMS-VoIP-Correlation item", "epsli.IMS_VoIP_Correlation_item_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_ims_iri,
      { "ims-iri", "epsli.ims_iri",
        FT_BYTES, BASE_NONE, NULL, 0,
        "IRI_to_IRI_Correlation", HFILL }},
    { &hf_epsli_ims_cc,
      { "ims-cc", "epsli.ims_cc_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IRI_to_CC_Correlation", HFILL }},
    { &hf_epsli_cc,
      { "cc", "epsli.cc",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_cc_item,
      { "cc item", "epsli.cc_item",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_iri,
      { "iri", "epsli.iri",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_gPRS_parameters,
      { "gPRS-parameters", "epsli.gPRS_parameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_pDP_address_allocated_to_the_target,
      { "pDP-address-allocated-to-the-target", "epsli.pDP_address_allocated_to_the_target",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_aPN,
      { "aPN", "epsli.aPN",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_100", HFILL }},
    { &hf_epsli_pDP_type,
      { "pDP-type", "epsli.pDP_type",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_2", HFILL }},
    { &hf_epsli_nSAPI,
      { "nSAPI", "epsli.nSAPI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_additionalIPaddress,
      { "additionalIPaddress", "epsli.additionalIPaddress",
        FT_UINT32, BASE_DEC, VALS(epsli_DataNodeAddress_vals), 0,
        "DataNodeAddress", HFILL }},
    { &hf_epsli_qosMobileRadio,
      { "qosMobileRadio", "epsli.qosMobileRadio",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_qosGn,
      { "qosGn", "epsli.qosGn",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_pDNAddressAllocation,
      { "pDNAddressAllocation", "epsli.pDNAddressAllocation",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_protConfigOptions,
      { "protConfigOptions", "epsli.protConfigOptions_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_attachType,
      { "attachType", "epsli.attachType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_ePSBearerIdentity,
      { "ePSBearerIdentity", "epsli.ePSBearerIdentity",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_detachType,
      { "detachType", "epsli.detachType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_rATType,
      { "rATType", "epsli.rATType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_failedBearerActivationReason,
      { "failedBearerActivationReason", "epsli.failedBearerActivationReason",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_ePSBearerQoS,
      { "ePSBearerQoS", "epsli.ePSBearerQoS",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_bearerActivationType,
      { "bearerActivationType", "epsli.bearerActivationType",
        FT_UINT32, BASE_DEC, VALS(epsli_TypeOfBearer_vals), 0,
        "TypeOfBearer", HFILL }},
    { &hf_epsli_aPN_AMBR,
      { "aPN-AMBR", "epsli.aPN_AMBR",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_procedureTransactionId,
      { "procedureTransactionId", "epsli.procedureTransactionId",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_linkedEPSBearerId,
      { "linkedEPSBearerId", "epsli.linkedEPSBearerId",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_tFT,
      { "tFT", "epsli.tFT",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_handoverIndication,
      { "handoverIndication", "epsli.handoverIndication_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_failedBearerModReason,
      { "failedBearerModReason", "epsli.failedBearerModReason",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_trafficAggregateDescription,
      { "trafficAggregateDescription", "epsli.trafficAggregateDescription",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_failedTAUReason,
      { "failedTAUReason", "epsli.failedTAUReason",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_failedEUTRANAttachReason,
      { "failedEUTRANAttachReason", "epsli.failedEUTRANAttachReason",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_servingMMEaddress,
      { "servingMMEaddress", "epsli.servingMMEaddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_bearerDeactivationType,
      { "bearerDeactivationType", "epsli.bearerDeactivationType",
        FT_UINT32, BASE_DEC, VALS(epsli_TypeOfBearer_vals), 0,
        "TypeOfBearer", HFILL }},
    { &hf_epsli_bearerDeactivationCause,
      { "bearerDeactivationCause", "epsli.bearerDeactivationCause",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_ePSlocationOfTheTarget,
      { "ePSlocationOfTheTarget", "epsli.ePSlocationOfTheTarget_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "EPSLocation", HFILL }},
    { &hf_epsli_pDNType,
      { "pDNType", "epsli.pDNType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_requestType,
      { "requestType", "epsli.requestType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_uEReqPDNConnFailReason,
      { "uEReqPDNConnFailReason", "epsli.uEReqPDNConnFailReason",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_extendedHandoverIndication,
      { "extendedHandoverIndication", "epsli.extendedHandoverIndication",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_uELocalIPAddress,
      { "uELocalIPAddress", "epsli.uELocalIPAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_uEUdpPort,
      { "uEUdpPort", "epsli.uEUdpPort",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_2", HFILL }},
    { &hf_epsli_tWANIdentifier,
      { "tWANIdentifier", "epsli.tWANIdentifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_tWANIdentifierTimestamp,
      { "tWANIdentifierTimestamp", "epsli.tWANIdentifierTimestamp",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_proSeRemoteUeContextConnected,
      { "proSeRemoteUeContextConnected", "epsli.proSeRemoteUeContextConnected",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RemoteUeContextConnected", HFILL }},
    { &hf_epsli_proSeRemoteUeContextDisconnected,
      { "proSeRemoteUeContextDisconnected", "epsli.proSeRemoteUeContextDisconnected",
        FT_BYTES, BASE_NONE, NULL, 0,
        "RemoteUeContextDisconnected", HFILL }},
    { &hf_epsli_secondaryRATUsageIndication,
      { "secondaryRATUsageIndication", "epsli.secondaryRATUsageIndication_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_fullyQualifiedTeid,
      { "fullyQualifiedTeid", "epsli.fullyQualifiedTeid",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_userLocationInfo,
      { "userLocationInfo", "epsli.userLocationInfo",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_39", HFILL }},
    { &hf_epsli_olduserLocationInfo,
      { "olduserLocationInfo", "epsli.olduserLocationInfo",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_39", HFILL }},
    { &hf_epsli_lastVisitedTAI,
      { "lastVisitedTAI", "epsli.lastVisitedTAI",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_5", HFILL }},
    { &hf_epsli_tAIlist,
      { "tAIlist", "epsli.tAIlist",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_7_97", HFILL }},
    { &hf_epsli_threeGPP2Bsid,
      { "threeGPP2Bsid", "epsli.threeGPP2Bsid",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_12", HFILL }},
    { &hf_epsli_uELocationTimestamp_01,
      { "uELocationTimestamp", "epsli.uELocationTimestamp",
        FT_UINT32, BASE_DEC, VALS(epsli_T_uELocationTimestamp_01_vals), 0,
        "T_uELocationTimestamp_01", HFILL }},
    { &hf_epsli_ueToNetwork,
      { "ueToNetwork", "epsli.ueToNetwork",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_251", HFILL }},
    { &hf_epsli_networkToUe,
      { "networkToUe", "epsli.networkToUe",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1_251", HFILL }},
    { &hf_epsli_RemoteUeContextConnected_item,
      { "RemoteUEContext", "epsli.RemoteUEContext_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_remoteUserID,
      { "remoteUserID", "epsli.remoteUserID",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_remoteUEIPInformation,
      { "remoteUEIPInformation", "epsli.remoteUEIPInformation",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_lifetime,
      { "lifetime", "epsli.lifetime",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_accessTechnologyType,
      { "accessTechnologyType", "epsli.accessTechnologyType",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_iPv6HomeNetworkPrefix,
      { "iPv6HomeNetworkPrefix", "epsli.iPv6HomeNetworkPrefix",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_20", HFILL }},
    { &hf_epsli_protConfigurationOption,
      { "protConfigurationOption", "epsli.protConfigurationOption",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_handoverIndication_01,
      { "handoverIndication", "epsli.handoverIndication",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_status,
      { "status", "epsli.status",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_255", HFILL }},
    { &hf_epsli_revocationTrigger,
      { "revocationTrigger", "epsli.revocationTrigger",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_255", HFILL }},
    { &hf_epsli_iPv4HomeAddress,
      { "iPv4HomeAddress", "epsli.iPv4HomeAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_iPv6careOfAddress,
      { "iPv6careOfAddress", "epsli.iPv6careOfAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_iPv4careOfAddress,
      { "iPv4careOfAddress", "epsli.iPv4careOfAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_servingNetwork,
      { "servingNetwork", "epsli.servingNetwork",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_3", HFILL }},
    { &hf_epsli_dHCPv4AddressAllocationInd,
      { "dHCPv4AddressAllocationInd", "epsli.dHCPv4AddressAllocationInd",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_1", HFILL }},
    { &hf_epsli_requestedIPv6HomePrefix,
      { "requestedIPv6HomePrefix", "epsli.requestedIPv6HomePrefix",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_25", HFILL }},
    { &hf_epsli_homeAddress,
      { "homeAddress", "epsli.homeAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_8", HFILL }},
    { &hf_epsli_iPv4careOfAddress_01,
      { "iPv4careOfAddress", "epsli.iPv4careOfAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_8", HFILL }},
    { &hf_epsli_iPv6careOfAddress_01,
      { "iPv6careOfAddress", "epsli.iPv6careOfAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_16", HFILL }},
    { &hf_epsli_hSS_AAA_address,
      { "hSS-AAA-address", "epsli.hSS_AAA_address",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_targetPDN_GW_Address,
      { "targetPDN-GW-Address", "epsli.targetPDN_GW_Address",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_homeAddress_01,
      { "homeAddress", "epsli.homeAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_careOfAddress,
      { "careOfAddress", "epsli.careOfAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_homeAgentAddress,
      { "homeAgentAddress", "epsli.homeAgentAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_code,
      { "code", "epsli.code",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_255", HFILL }},
    { &hf_epsli_foreignDomainAddress,
      { "foreignDomainAddress", "epsli.foreignDomainAddress",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING_SIZE_4", HFILL }},
    { &hf_epsli_MediaDecryption_info_item,
      { "CCKeyInfo", "epsli.CCKeyInfo_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_cCCSID,
      { "cCCSID", "epsli.cCCSID",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_cCDecKey,
      { "cCDecKey", "epsli.cCDecKey",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_cCSalt,
      { "cCSalt", "epsli.cCSalt",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_packetDataHeader,
      { "packetDataHeader", "epsli.packetDataHeader",
        FT_UINT32, BASE_DEC, VALS(epsli_PacketDataHeaderReport_vals), 0,
        "PacketDataHeaderReport", HFILL }},
    { &hf_epsli_packetDataSummary,
      { "packetDataSummary", "epsli.packetDataSummary",
        FT_UINT32, BASE_DEC, NULL, 0,
        "PacketDataSummaryReport", HFILL }},
    { &hf_epsli_packetDataHeaderMapped,
      { "packetDataHeaderMapped", "epsli.packetDataHeaderMapped_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_packetDataHeaderCopy,
      { "packetDataHeaderCopy", "epsli.packetDataHeaderCopy_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_sourceIPAddress,
      { "sourceIPAddress", "epsli.sourceIPAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IPAddress", HFILL }},
    { &hf_epsli_sourcePortNumber,
      { "sourcePortNumber", "epsli.sourcePortNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_destinationIPAddress,
      { "destinationIPAddress", "epsli.destinationIPAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IPAddress", HFILL }},
    { &hf_epsli_destinationPortNumber,
      { "destinationPortNumber", "epsli.destinationPortNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_transportProtocol,
      { "transportProtocol", "epsli.transportProtocol",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_packetsize,
      { "packetsize", "epsli.packetsize",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_flowLabel,
      { "flowLabel", "epsli.flowLabel",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_packetCount,
      { "packetCount", "epsli.packetCount",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_direction_01,
      { "direction", "epsli.direction",
        FT_UINT32, BASE_DEC, VALS(epsli_TPDU_direction_vals), 0,
        "TPDU_direction", HFILL }},
    { &hf_epsli_headerCopy,
      { "headerCopy", "epsli.headerCopy",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_PacketDataSummaryReport_item,
      { "PacketFlowSummary", "epsli.PacketFlowSummary_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_summaryPeriod,
      { "summaryPeriod", "epsli.summaryPeriod_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ReportInterval", HFILL }},
    { &hf_epsli_sumOfPacketSizes,
      { "sumOfPacketSizes", "epsli.sumOfPacketSizes",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_packetDataSummaryReason,
      { "packetDataSummaryReason", "epsli.packetDataSummaryReason",
        FT_UINT32, BASE_DEC, VALS(epsli_ReportReason_vals), 0,
        "ReportReason", HFILL }},
    { &hf_epsli_firstPacketTimeStamp,
      { "firstPacketTimeStamp", "epsli.firstPacketTimeStamp",
        FT_UINT32, BASE_DEC, VALS(epsli_TimeStamp_vals), 0,
        "TimeStamp", HFILL }},
    { &hf_epsli_lastPacketTimeStamp,
      { "lastPacketTimeStamp", "epsli.lastPacketTimeStamp",
        FT_UINT32, BASE_DEC, VALS(epsli_TimeStamp_vals), 0,
        "TimeStamp", HFILL }},
    { &hf_epsli_rfc2868ValueField,
      { "rfc2868ValueField", "epsli.rfc2868ValueField",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_nativeIPSec,
      { "nativeIPSec", "epsli.nativeIPSec_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_new_MSISDN,
      { "new-MSISDN", "epsli.new_MSISDN_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_A_MSISDN,
      { "new-A-MSISDN", "epsli.new_A_MSISDN_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_MSISDN,
      { "old-MSISDN", "epsli.old_MSISDN_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_A_MSISDN,
      { "old-A-MSISDN", "epsli.old_A_MSISDN_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_IMSI,
      { "new-IMSI", "epsli.new_IMSI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_IMSI,
      { "old-IMSI", "epsli.old_IMSI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_IMEI,
      { "new-IMEI", "epsli.new_IMEI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_IMEI,
      { "old-IMEI", "epsli.old_IMEI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_IMPI,
      { "new-IMPI", "epsli.new_IMPI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_IMPI,
      { "old-IMPI", "epsli.old_IMPI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_SIP_URI,
      { "new-SIP-URI", "epsli.new_SIP_URI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_SIP_URI,
      { "old-SIP-URI", "epsli.old_SIP_URI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_new_TEL_URI,
      { "new-TEL-URI", "epsli.new_TEL_URI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_old_TEL_URI,
      { "old-TEL-URI", "epsli.old_TEL_URI_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PartyInformation", HFILL }},
    { &hf_epsli_current_Serving_MME_Address,
      { "current-Serving-MME-Address", "epsli.current_Serving_MME_Address_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "DataNodeIdentifier", HFILL }},
    { &hf_epsli_previous_Serving_System_Identifier,
      { "previous-Serving-System-Identifier", "epsli.previous_Serving_System_Identifier",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_epsli_previous_Serving_MME_Address,
      { "previous-Serving-MME-Address", "epsli.previous_Serving_MME_Address_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "DataNodeIdentifier", HFILL }},
    { &hf_epsli_reason_CodeAVP,
      { "reason-CodeAVP", "epsli.reason_CodeAVP",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_server_AssignmentType,
      { "server-AssignmentType", "epsli.server_AssignmentType",
        FT_INT32, BASE_DEC, NULL, 0,
        "INTEGER", HFILL }},
    { &hf_epsli_cipher,
      { "cipher", "epsli.cipher",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_cryptoContext,
      { "cryptoContext", "epsli.cryptoContext",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_key,
      { "key", "epsli.key",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_keyEncoding,
      { "keyEncoding", "epsli.keyEncoding",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_salt,
      { "salt", "epsli.salt",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCOther,
      { "pTCOther", "epsli.pTCOther",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_abandonCause,
      { "abandonCause", "epsli.abandonCause",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_accessPolicyFailure,
      { "accessPolicyFailure", "epsli.accessPolicyFailure",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_accessPolicyType,
      { "accessPolicyType", "epsli.accessPolicyType_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_alertIndicator,
      { "alertIndicator", "epsli.alertIndicator",
        FT_UINT32, BASE_DEC, VALS(epsli_AlertIndicator_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_associatePresenceStatus,
      { "associatePresenceStatus", "epsli.associatePresenceStatus_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_bearer_capability,
      { "bearer-capability", "epsli.bearer_capability",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_broadcastIndicator,
      { "broadcastIndicator", "epsli.broadcastIndicator",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_contactID,
      { "contactID", "epsli.contactID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_emergency,
      { "emergency", "epsli.emergency",
        FT_UINT32, BASE_DEC, VALS(epsli_Emergency_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_emergencyGroupState,
      { "emergencyGroupState", "epsli.emergencyGroupState_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_pTCType,
      { "pTCType", "epsli.pTCType",
        FT_UINT32, BASE_DEC, VALS(epsli_PTCType_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_failureCode,
      { "failureCode", "epsli.failureCode",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_floorActivity,
      { "floorActivity", "epsli.floorActivity_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_floorSpeakerID,
      { "floorSpeakerID", "epsli.floorSpeakerID_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PTCAddress", HFILL }},
    { &hf_epsli_groupAdSender,
      { "groupAdSender", "epsli.groupAdSender",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_groupAuthRule,
      { "groupAuthRule", "epsli.groupAuthRule",
        FT_UINT32, BASE_DEC, VALS(epsli_GroupAuthRule_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_groupCharacteristics,
      { "groupCharacteristics", "epsli.groupCharacteristics",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_holdRetrieveInd,
      { "holdRetrieveInd", "epsli.holdRetrieveInd",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_imminentPerilInd,
      { "imminentPerilInd", "epsli.imminentPerilInd",
        FT_UINT32, BASE_DEC, VALS(epsli_ImminentPerilInd_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_implicitFloorReq,
      { "implicitFloorReq", "epsli.implicitFloorReq",
        FT_UINT32, BASE_DEC, VALS(epsli_ImplicitFloorReq_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_initiationCause,
      { "initiationCause", "epsli.initiationCause",
        FT_UINT32, BASE_DEC, VALS(epsli_InitiationCause_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_invitationCause,
      { "invitationCause", "epsli.invitationCause",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_iPAPartyID,
      { "iPAPartyID", "epsli.iPAPartyID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_iPADirection,
      { "iPADirection", "epsli.iPADirection",
        FT_UINT32, BASE_DEC, VALS(epsli_IPADirection_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_listManagementAction,
      { "listManagementAction", "epsli.listManagementAction",
        FT_UINT32, BASE_DEC, VALS(epsli_ListManagementAction_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_listManagementFailure,
      { "listManagementFailure", "epsli.listManagementFailure",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_listManagementType,
      { "listManagementType", "epsli.listManagementType",
        FT_UINT32, BASE_DEC, VALS(epsli_ListManagementType_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_maxTBTime,
      { "maxTBTime", "epsli.maxTBTime",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_mCPTTGroupID,
      { "mCPTTGroupID", "epsli.mCPTTGroupID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_mCPTTID,
      { "mCPTTID", "epsli.mCPTTID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_mCPTTInd,
      { "mCPTTInd", "epsli.mCPTTInd",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_mCPTTOrganizationName,
      { "mCPTTOrganizationName", "epsli.mCPTTOrganizationName",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_mediaStreamAvail,
      { "mediaStreamAvail", "epsli.mediaStreamAvail",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_priority_Level,
      { "priority-Level", "epsli.priority_Level",
        FT_UINT32, BASE_DEC, VALS(epsli_Priority_Level_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_preEstSessionID,
      { "preEstSessionID", "epsli.preEstSessionID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_preEstStatus,
      { "preEstStatus", "epsli.preEstStatus",
        FT_UINT32, BASE_DEC, VALS(epsli_PreEstStatus_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_pTCGroupID,
      { "pTCGroupID", "epsli.pTCGroupID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCIDList,
      { "pTCIDList", "epsli.pTCIDList",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCMediaCapability,
      { "pTCMediaCapability", "epsli.pTCMediaCapability",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCOriginatingId,
      { "pTCOriginatingId", "epsli.pTCOriginatingId",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCParticipants,
      { "pTCParticipants", "epsli.pTCParticipants",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCParty,
      { "pTCParty", "epsli.pTCParty",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCPartyDrop,
      { "pTCPartyDrop", "epsli.pTCPartyDrop",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCSessionInfo,
      { "pTCSessionInfo", "epsli.pTCSessionInfo",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCServerURI,
      { "pTCServerURI", "epsli.pTCServerURI",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCUserAccessPolicy,
      { "pTCUserAccessPolicy", "epsli.pTCUserAccessPolicy",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_pTCAddress,
      { "pTCAddress", "epsli.pTCAddress_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_queuedFloorControl,
      { "queuedFloorControl", "epsli.queuedFloorControl",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_queuedPosition,
      { "queuedPosition", "epsli.queuedPosition",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_registrationRequest,
      { "registrationRequest", "epsli.registrationRequest",
        FT_UINT32, BASE_DEC, VALS(epsli_RegistrationRequest_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_registrationOutcome,
      { "registrationOutcome", "epsli.registrationOutcome",
        FT_UINT32, BASE_DEC, VALS(epsli_RegistrationOutcome_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_retrieveID,
      { "retrieveID", "epsli.retrieveID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_rTPSetting,
      { "rTPSetting", "epsli.rTPSetting_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_talkBurstPriority,
      { "talkBurstPriority", "epsli.talkBurstPriority",
        FT_UINT32, BASE_DEC, VALS(epsli_Priority_Level_vals), 0,
        "Priority_Level", HFILL }},
    { &hf_epsli_talkBurstReason,
      { "talkBurstReason", "epsli.talkBurstReason",
        FT_STRING, BASE_NONE, NULL, 0,
        "Talk_burst_reason_code", HFILL }},
    { &hf_epsli_talkburstControlSetting,
      { "talkburstControlSetting", "epsli.talkburstControlSetting_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_targetPresenceStatus,
      { "targetPresenceStatus", "epsli.targetPresenceStatus",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_port_Number,
      { "port-Number", "epsli.port_Number",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_userAccessPolicyAttempt,
      { "userAccessPolicyAttempt", "epsli.userAccessPolicyAttempt",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_groupAuthorizationRulesAttempt,
      { "groupAuthorizationRulesAttempt", "epsli.groupAuthorizationRulesAttempt",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_userAccessPolicyQuery,
      { "userAccessPolicyQuery", "epsli.userAccessPolicyQuery",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_groupAuthorizationRulesQuery,
      { "groupAuthorizationRulesQuery", "epsli.groupAuthorizationRulesQuery",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_userAccessPolicyResult,
      { "userAccessPolicyResult", "epsli.userAccessPolicyResult",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_groupAuthorizationRulesResult,
      { "groupAuthorizationRulesResult", "epsli.groupAuthorizationRulesResult",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_presenceID,
      { "presenceID", "epsli.presenceID",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_presenceType,
      { "presenceType", "epsli.presenceType",
        FT_UINT32, BASE_DEC, VALS(epsli_PresenceType_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_presenceStatus,
      { "presenceStatus", "epsli.presenceStatus",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_clientEmergencyState,
      { "clientEmergencyState", "epsli.clientEmergencyState",
        FT_UINT32, BASE_DEC, VALS(epsli_T_clientEmergencyState_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_groupEmergencyState,
      { "groupEmergencyState", "epsli.groupEmergencyState",
        FT_UINT32, BASE_DEC, VALS(epsli_T_groupEmergencyState_vals), 0,
        NULL, HFILL }},
    { &hf_epsli_tBCP_Request,
      { "tBCP-Request", "epsli.tBCP_Request",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Granted,
      { "tBCP-Granted", "epsli.tBCP_Granted",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Deny,
      { "tBCP-Deny", "epsli.tBCP_Deny",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Queued,
      { "tBCP-Queued", "epsli.tBCP_Queued",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Release,
      { "tBCP-Release", "epsli.tBCP_Release",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Revoke,
      { "tBCP-Revoke", "epsli.tBCP_Revoke",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Taken,
      { "tBCP-Taken", "epsli.tBCP_Taken",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_tBCP_Idle,
      { "tBCP-Idle", "epsli.tBCP_Idle",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_uri,
      { "uri", "epsli.uri",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_privacy_setting,
      { "privacy-setting", "epsli.privacy_setting",
        FT_BOOLEAN, BASE_NONE, NULL, 0,
        "BOOLEAN", HFILL }},
    { &hf_epsli_privacy_alias,
      { "privacy-alias", "epsli.privacy_alias",
        FT_STRING, BASE_NONE, NULL, 0,
        "VisibleString", HFILL }},
    { &hf_epsli_nickname,
      { "nickname", "epsli.nickname",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_ip_address,
      { "ip-address", "epsli.ip_address_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "IPAddress", HFILL }},
    { &hf_epsli_port_number,
      { "port-number", "epsli.port_number",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_talk_BurstControlProtocol,
      { "talk-BurstControlProtocol", "epsli.talk_BurstControlProtocol",
        FT_STRING, BASE_NONE, NULL, 0,
        "UTF8String", HFILL }},
    { &hf_epsli_talk_Burst_parameters,
      { "talk-Burst-parameters", "epsli.talk_Burst_parameters",
        FT_UINT32, BASE_DEC, NULL, 0,
        "T_talk_Burst_parameters", HFILL }},
    { &hf_epsli_talk_Burst_parameters_item,
      { "talk-Burst-parameters item", "epsli.talk_Burst_parameters_item",
        FT_STRING, BASE_NONE, NULL, 0,
        "VisibleString", HFILL }},
    { &hf_epsli_tBCP_PortNumber,
      { "tBCP-PortNumber", "epsli.tBCP_PortNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_uLIC_header,
      { "uLIC-header", "epsli.uLIC_header_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_payload,
      { "payload", "epsli.payload",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_epsli_hi3DomainId,
      { "hi3DomainId", "epsli.hi3DomainId",
        FT_OID, BASE_NONE, NULL, 0,
        "OBJECT_IDENTIFIER", HFILL }},
    { &hf_epsli_lIID,
      { "lIID", "epsli.lIID",
        FT_BYTES, BASE_NONE, NULL, 0,
        "LawfulInterceptionIdentifier", HFILL }},
    { &hf_epsli_correlation_Number,
      { "correlation-Number", "epsli.correlation_Number",
        FT_BYTES, BASE_NONE, NULL, 0,
        "EPSCorrelationNumber", HFILL }},
    { &hf_epsli_sequence_number,
      { "sequence-number", "epsli.sequence_number",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INTEGER_0_65535", HFILL }},
    { &hf_epsli_t_PDU_direction,
      { "t-PDU-direction", "epsli.t_PDU_direction",
        FT_UINT32, BASE_DEC, VALS(epsli_TPDU_direction_vals), 0,
        "TPDU_direction", HFILL }},
    { &hf_epsli_national_HI3_ASN1parameters,
      { "national-HI3-ASN1parameters", "epsli.national_HI3_ASN1parameters_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},

/*--- End of included file: packet-epsli-hfarr.c ---*/
#line 139 "./asn1/epsli/packet-epsli-template.c"
  };

  /* List of subtrees */
  static gint *ett[] = {
    &ett_iri,

/*--- Included file: packet-epsli-ettarr.c ---*/
#line 1 "./asn1/epsli/packet-epsli-ettarr.c"
    &ett_epsli_TimeStamp,
    &ett_epsli_LocalTimeStamp,
    &ett_epsli_Network_Identifier,
    &ett_epsli_Network_Element_Identifier,
    &ett_epsli_National_Parameters,
    &ett_epsli_National_HI2_ASN1parameters,
    &ett_epsli_DataNodeAddress,
    &ett_epsli_IPAddress,
    &ett_epsli_IP_value,
    &ett_epsli_CivicAddress,
    &ett_epsli_SET_OF_DetailedCivicAddress,
    &ett_epsli_DetailedCivicAddress,
    &ett_epsli_ExtendedLocParameters,
    &ett_epsli_T_mapData,
    &ett_epsli_T_altitude,
    &ett_epsli_T_motionStateList,
    &ett_epsli_T_secondaryMotionState,
    &ett_epsli_T_floor,
    &ett_epsli_EpsIRIContent,
    &ett_epsli_IRI_Parameters,
    &ett_epsli_SET_SIZE_1_10_OF_PartyInformation,
    &ett_epsli_SEQUENCE_OF_PANI_Header_Info,
    &ett_epsli_SEQUENCE_OF_PartyInformation,
    &ett_epsli_SEQUENCE_OF_AdditionalCellID,
    &ett_epsli_DataNodeIdentifier,
    &ett_epsli_PANI_Header_Info,
    &ett_epsli_PANI_Location,
    &ett_epsli_PartyInformation,
    &ett_epsli_T_partyIdentity,
    &ett_epsli_Location,
    &ett_epsli_T_uELocationTimestamp,
    &ett_epsli_AdditionalCellID,
    &ett_epsli_PLMNID,
    &ett_epsli_NCGI,
    &ett_epsli_GSMLocation,
    &ett_epsli_T_geoCoordinates,
    &ett_epsli_T_utmCoordinates,
    &ett_epsli_T_utmRefCoordinates,
    &ett_epsli_UMTSLocation,
    &ett_epsli_GeographicalCoordinates,
    &ett_epsli_GA_Point,
    &ett_epsli_GA_PointWithUnCertainty,
    &ett_epsli_GA_Polygon,
    &ett_epsli_GA_Polygon_item,
    &ett_epsli_SMS_report,
    &ett_epsli_T_sMS_Contents,
    &ett_epsli_CorrelationValues,
    &ett_epsli_T_both_IRI_CC,
    &ett_epsli_IMS_VoIP_Correlation,
    &ett_epsli_IMS_VoIP_Correlation_item,
    &ett_epsli_IRI_to_CC_Correlation,
    &ett_epsli_T_cc,
    &ett_epsli_Services_Data_Information,
    &ett_epsli_GPRS_parameters,
    &ett_epsli_UmtsQos,
    &ett_epsli_EPS_GTPV2_SpecificParameters,
    &ett_epsli_EPSLocation,
    &ett_epsli_T_uELocationTimestamp_01,
    &ett_epsli_ProtConfigOptions,
    &ett_epsli_RemoteUeContextConnected,
    &ett_epsli_RemoteUEContext,
    &ett_epsli_EPS_PMIP_SpecificParameters,
    &ett_epsli_EPS_DSMIP_SpecificParameters,
    &ett_epsli_EPS_MIP_SpecificParameters,
    &ett_epsli_MediaDecryption_info,
    &ett_epsli_CCKeyInfo,
    &ett_epsli_PacketDataHeaderInformation,
    &ett_epsli_PacketDataHeaderReport,
    &ett_epsli_PacketDataHeaderMapped,
    &ett_epsli_PacketDataHeaderCopy,
    &ett_epsli_PacketDataSummaryReport,
    &ett_epsli_PacketFlowSummary,
    &ett_epsli_ReportInterval,
    &ett_epsli_TunnelProtocol,
    &ett_epsli_Change_Of_Target_Identity,
    &ett_epsli_Current_Previous_Systems,
    &ett_epsli_DeregistrationReason,
    &ett_epsli_PTCEncryptionInfo,
    &ett_epsli_PTC,
    &ett_epsli_AccessPolicyType,
    &ett_epsli_AssociatePresenceStatus,
    &ett_epsli_EmergencyGroupState,
    &ett_epsli_FloorActivity,
    &ett_epsli_PTCAddress,
    &ett_epsli_RTPSetting,
    &ett_epsli_TalkburstControlSetting,
    &ett_epsli_T_talk_Burst_parameters,
    &ett_epsli_CC_PDU,
    &ett_epsli_ULIC_header,
    &ett_epsli_National_HI3_ASN1parameters,

/*--- End of included file: packet-epsli-ettarr.c ---*/
#line 145 "./asn1/epsli/packet-epsli-template.c"
  };

  /* Register protocol */
  proto_epsli = proto_register_protocol(PNAME, PSNAME, PFNAME);

  /* Register fields and subtrees */
  proto_register_field_array(proto_epsli, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  //epsli_handle = register_dissector("epsli", dissect_CC_PDU_PDU, proto_epsli);
}

void proto_reg_handoff_epsli(void) {

  ip_handle=find_dissector("ip");
  ipv6_handle=find_dissector("ipv6");

  //dissector_add_uint_range_with_preference("udp.port", "", epsli_handle);
  heur_dissector_add("tcp", dissect_x2IRI_heur, "EPS LI x2 over TCP", "epslix2", proto_epsli, HEURISTIC_ENABLE);
  heur_dissector_add("tcp", dissect_x3cc_heur, "EPS LI x3 over TCP", "epslix3_tcp", proto_epsli, HEURISTIC_ENABLE);
  heur_dissector_add("udp", dissect_x3cc_heur, "EPS LI x3 over UDP", "epslix3_udp", proto_epsli, HEURISTIC_ENABLE);

}
