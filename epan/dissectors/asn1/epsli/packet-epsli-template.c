/* packet-epsli-template.c
 * Routines for Lawful Interception X2 xIRI event dissection
 *
 * See 3GPP TS33.128.
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <epan/packet.h>
#include <epan/conversation.h>
#include <epan/asn1.h>

#include "packet-ber.h"

#define PNAME  "EPS Lawful Interception"
#define PSNAME "EPSLI"
#define PFNAME "epsli"   

void proto_reg_handoff_epsli(void);
void proto_register_epsli(void);

/* Initialize the protocol and registered fields */
static int proto_epsli = -1;
static gint ett_iri = -1;
static dissector_handle_t ip_handle;
static dissector_handle_t ipv6_handle;
//static dissector_handle_t epsli_handle = NULL;


#include "packet-epsli-hf.c"

#include "packet-epsli-ett.c"

#include "packet-epsli-fn.c"


static gboolean
dissect_x2IRI_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
  proto_item *iri_item = NULL;
  proto_tree *iri_tree = NULL;
  tvbuff_t   *next_tvb;
  guint8 firstByte;
  guint32 len, totalLen, offset=0; 

  totalLen=tvb_reported_length(tvb);
  do{
    /* the total len should at least xx bytes */
    //if (totalLen<20)
      //return FALSE;

    /* the TPKT header */
    firstByte=tvb_get_guint8(tvb, offset);
    if (firstByte != 3)
      return FALSE;

    if (tvb_get_guint8(tvb, offset+1)!=0)
      return FALSE;
    
    len=tvb_get_ntohs(tvb, offset+2);
    if(len>totalLen-offset)
      return FALSE;

    iri_item = proto_tree_add_item(tree, proto_epsli, tvb, offset, offset+len, ENC_NA);
    iri_tree = proto_item_add_subtree(iri_item, ett_iri);
    offset += 4;

    next_tvb = tvb_new_subset_length(tvb, offset, tvb_reported_length_remaining(tvb, offset));
    offset+=dissect_EpsIRIContent_PDU(next_tvb, pinfo, iri_tree, data);

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "EPSLI");
    col_clear_fence(pinfo->cinfo, COL_INFO);
    col_clear(pinfo->cinfo, COL_INFO);
    col_set_str(pinfo->cinfo, COL_INFO, "x2 PDU");
  }while(offset<totalLen); 
  
  return TRUE;
}

static gboolean
dissect_x3cc_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
  proto_item *iri_item = NULL;
  proto_tree *iri_tree = NULL;
  tvbuff_t   *next_tvb;
  guint8 firstByte, secondByte;
  guint32 len, totalLen, offset=0; 

  totalLen=tvb_reported_length(tvb);
  do{
    firstByte=tvb_get_guint8(tvb, offset);
    if (firstByte != 0x30)
      return FALSE;
    
    secondByte=tvb_get_guint8(tvb, offset+1);
    if (secondByte != 0x82)
      return FALSE;
    
    len=tvb_get_ntohs(tvb, offset+2);
    if(len > totalLen-offset-4){
      if (pinfo->ptype==PT_TCP){
        /* TCP reassemble */
        pinfo->desegment_offset = offset;
        pinfo->desegment_len = DESEGMENT_ONE_MORE_SEGMENT;
        break;
      }
      else
        return FALSE;
    }   

    iri_item = proto_tree_add_item(tree, proto_epsli, tvb, offset, offset+len+4, ENC_NA);
    iri_tree = proto_item_add_subtree(iri_item, ett_iri);
    next_tvb = tvb_new_subset_length(tvb, offset, tvb_reported_length_remaining(tvb, offset));
    offset+=dissect_CC_PDU_PDU(next_tvb, pinfo, iri_tree, data);
  }while(offset<totalLen);    
  
  col_set_str(pinfo->cinfo, COL_PROTOCOL, "EPSLI");
  col_clear_fence(pinfo->cinfo, COL_INFO);
  col_clear(pinfo->cinfo, COL_INFO);
  col_set_str(pinfo->cinfo, COL_INFO, "x3 PDU");

  return TRUE;
}

/*--- proto_register_epsli -------------------------------------------*/
void proto_register_epsli(void) {

  /* List of fields */
  static hf_register_info hf[] = {
#include "packet-epsli-hfarr.c"
  };

  /* List of subtrees */
  static gint *ett[] = {
    &ett_iri,
#include "packet-epsli-ettarr.c"
  };

  /* Register protocol */
  proto_epsli = proto_register_protocol(PNAME, PSNAME, PFNAME);

  /* Register fields and subtrees */
  proto_register_field_array(proto_epsli, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  //epsli_handle = register_dissector("epsli", dissect_CC_PDU_PDU, proto_epsli);
}

void proto_reg_handoff_epsli(void) {

  ip_handle=find_dissector("ip");
  ipv6_handle=find_dissector("ipv6");

  //dissector_add_uint_range_with_preference("udp.port", "", epsli_handle);
  heur_dissector_add("tcp", dissect_x2IRI_heur, "EPS LI x2 over TCP", "epslix2", proto_epsli, HEURISTIC_ENABLE);
  heur_dissector_add("tcp", dissect_x3cc_heur, "EPS LI x3 over TCP", "epslix3_tcp", proto_epsli, HEURISTIC_ENABLE);
  heur_dissector_add("udp", dissect_x3cc_heur, "EPS LI x3 over UDP", "epslix3_udp", proto_epsli, HEURISTIC_ENABLE);

}
